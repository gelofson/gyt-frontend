if (!TN) var TN= {};
if (!TN.createStory) TN.createStory= {};

(function($createStory){
	var publishInProgress = false;
	var resetAddress = false;
	var modalsLoadedInDOM = false;
	
	$createStory.close = function(){
		
		if( !!resetAddress ){
			location.href = 'index.html';
		}
		
		publishInProgress = false;
		$createStory.createStoryModalElem.trigger('reveal:close');

		// Clear out text forms and image, hide counters
		$('#csImage').attr('src', '');
		$('#HfamilyTab span, #csStoryHeadlineCounter, #csOpEdCounter').hide();
		$('#csHeadlineInput, .tabs-content input, .tabs-content textarea').val('');
	};
	
	function showCreateStory(imageUrl, storyUrl, resetAddressBar, userLoggedIn){
		resetAddress = resetAddressBar;
		var createImageUrl = '';
		var bodyElem = $('body');
		var createStoryModalHtml = '<div id="createStoryModal" class="clipthis reveal-modal" >\
		    <div class="clipthis-loging block" style="display:none">\
		    	<h2>Log in to continue</h2>\
		        <a class="close-reveal-modal"></a>\
		        <!--form action="#"-->\
		        	<div class="block">\
		        	<label>Username</label>\
		            <input id="csUsername" type="text" name="username text" class="clipthis-login_txtBox">\
		            </div>\
		            <div class="block">\
		            <label>Password</label>\
		            <input id="csPassword" type="password" name="username text" class="clipthis-login_txtBox">\
		            </div>\
		            <div class="block">\
		            	<ul class="clipthisFormList no-bullet">\
		                	<li style="margin-top:2px"><a href="#">Cancel</a></li>\
		                    <li><input type="submit" value="login" name="submit" class="clipthis-login_submitBox" ></li>\
		                </ul>\
		            </div>\
		        <!--/form-->\
		    </div>\
		   	<a class="close-reveal-modal"></a>\
		    <div class="clipthis-content">\
		      	<form class="custom" method="post" enctype="multipart/form-data" id="csFoundationWrapperForm">\
				<input type="hidden" id="videoIdInput"/>\
		      	<div class="clipthis-content_Header">\
		      	<h3 class="clipPostRevealHeader"></h3>\
		          	<input id="csHeadlineInput" type="text" placeholder="Enter Headline Here..." name="StoryHeadline" class="CT-HeadertxtBox">\
		          	<span class="charCounter" style="float: right; position: absolute; right: 44px; top: 75px; z-index: 99; display:none; color:#CCCCCC" id="csStoryHeadlineCounter">50</span>\
		        </div>\
		        <div class="clipthis-content_Body block">\
		          	<div id="csEditPhoto" class="clipthis-UserimageContainer" style="display:none;">\
		              	<div class="clipthis-UserimageContainerInner" style="width: 150px; height: 110px; overflow:hidden">\
		              		<label for="csEditPhotoInput" id="csEditPhotoLabel">\
		              			<img id="csImage" src="" alt="user image" style="max-width: 150px; max-height: 110px">\
		              		</label>\
		              	</div>\
		                <a class="userImage-shadow"></a>\
						<input style="position: absolute; left:-9999em" id="csEditPhotoInput" name="file" type="file" accept="image/*"/> \
		            </div>\
					<iframe id="videoCont" class="clipthis-UserimageContainer" width="150px" height="110px" src="http://www.youtube.com/v/04b1-dEQeeo" style="display:none;"></iframe>\
		            <div class="clipthis-StoryOpt block">\
		                <div class="custom dropdown rounded" id="customDropdown">\
		                    <a href="#" class="current rounded"></a>\
		                    <a href="#" class="selector"></a>\
		                    <ul>\
		                      	<li></li>\
		                    </ul>\
		                </div>\
		                <a href="javascript:;" id="publishButton" class="button publishBtn radius">Publish or Perish</a>\
		                <ul class="clipthis-social no-bullet">\
		                  	<li id="facebookPost"><input type="checkbox" name="checkbox"> facebook</li>\
		                    <li id="twitterPost"><input type="checkbox" name="checkbox"> twitter</li>\
		                </ul>\
		            </div>\
		        </div>\
		            <div class="clipthis-storyContent">\
		             	<p class="text-center">Tell Your Story (optional)</p>\
		                <dl class="tabs two-up block contained">\
		                    <dd class="active"><a id="writeStoryTab" href="#Hfamily">The 5 W’s &amp; How</a></dd>\
		                    <dd><a id="writeOpEdTab" href="#Freefamily">Editorial</a></dd>\
		                </dl>\
		                <ul class="tabs-content contained">\
		                  	<li class="active" id="HfamilyTab">\
			                    <div class="csTextInputBox">\
			                    	<input id="csWhoInput" type="text" name="Who" placeholder="Who? The Fire Department" class="CT-txtBox small">\
			                    	<span class="charCounter" style="float:right; position:relative; right:15px; top:-34px; display:none; color:#CCCCCC" id="csWhoCounter">99</span>\
			                    </div>\
			                    <div class="csTextInputBox">\
			                    	<input id="csWhatInput" type="text" name="What" placeholder="What? Rescued Top Cat" class="CT-txtBox small">\
			                    	<span class="charCounter" style="float:right; position:relative; right:15px; top:-34px; display:none; color:#CCCCCC" id="csWhatCounter">99</span>\
			                    </div>\
			                    <div class="locationBox">\
			                      <input id="csWhereInput" type="text" name="Where" placeholder="Where? from the top of the old elm tree" class="CT-txtBox big">\
			                        <a class="locationPin"></a>\
			                        <span class="charCounter" style="float:right; position:absolute; right:50px; top:15px; z-index: 99; display:none; color:#CCCCCC" id="csWhereCounter">199</span>\
			                    </div>\
			                    <div class="expandedWhereFields">\
				                    <input type="text" name="address" class="CT-txtBox small" id="csAddressInput" placeholder="Address">\
									<input type="text" name="city" class="CT-txtBox small" id="csCityInput" placeholder="City">\
									<div class="row stateZipcodeWrapper" style="margin-bottom:0px;">\
									    <div class="six column csTextInputBox" style="padding-left:0px; padding-right:7px">\
									        <input type="text" name="stateCountry" class="CT-txtBox small" style="" id="csStateCountryInput" placeholder="State/Country">\
									    </div>\
									    <div class="six column csTextInputBox" style="padding-right:0px; padding-left:7px">\
									        <input type="text" name="zipcode" class="CT-txtBox small" style="" id="csZipcodeInput" placeholder="Zipcode">\
									    </div>\
									</div>\
								</div>\
			                    <div class="csTextInputBox">\
			                    	<input id="csWhenInput" type="text" name="When" placeholder="When? at 2am this morning" class="CT-txtBox big">\
			                    	<span class="charCounter" style="float:right; position:relative; right:15px; top:-34px; display:none; color:#CCCCCC" id="csWhenCounter">99</span>\
			                    </div>\
			                    <div class="csTextInputBox">\
			                    	<input id="csWhyInput" type="text" name="Why" placeholder="Why? because the neighbors kept calling about all the meowing" class="CT-txtBox big">\
			                    	<span class="charCounter" style="float:right; position:relative; right:15px; top:-34px; display:none; color:#CCCCCC" id="csWhyCounter">99</span>\
			                    </div>\
			                    <div class="csTextInputBox">\
			                    	<input id="csHowInput" type="text" name="How" placeholder="How? with the use of their tall fire ladders" class="CT-txtBox big">\
			                    	<span class="charCounter" style="float:right; position:relative; right:15px; top:-34px; display:none; color:#CCCCCC" id="csHowCounter">99</span>\
			                    </div>\
		                  	</li>\
		                  	<li id="FreefamilyTab">\
			                    <textarea id="csOpEdInput" name="OpEd" class="clipthis-textarea" placeholder="Tell Your Story"></textarea>\
			                    <span class="charCounter" style="float: right; position: relative; right: 30px; top: -37px; color:#CCCCCC" id="csOpEdCounter"></span>\
		                  	</li>\
		                </ul>\
		            </div>\
		        </form>\
		    </div>\
		</div>';

		var uploadPicModalHtml = '<div id="UploadPicModal" class="clipthis reveal-modal" style="">\
			<a class="close-reveal-modal"></a>\
			<div class="clipthis-content">\
				<form id="csChoosePhotoForm" method="post" enctype="multipart/form-data" class="custom">\
					<div class="uploadphoto_Header">\
						<h3 class="clipPostRevealHeader" style="margin-bottom: 29px"></h3>\
						<div class="locationBox">\
							<label for="csChoosePhotoInput" id="csChoosePhotoInputFakeLabel">\
								<input id="csChoosePhotoInputFake" type="button" name="file" type="button" class="button publishBtn radius" style="position:absolute; top:1px; right: 20%; padding: 0" value="Upload News photo">\
				        	</label>\
				        	<input placeholder="" class="" id="csChoosePhotoInput" name="file" type="file" accept="image/*" style="position: absolute; left:-9999em">\
				        </div>\
						<p class="text-center" style="margin-top: 103px">Or enter a Youtube URL...</p>\
						<div class="row">\
							<div class="eight columns">\
								<input id="videoUrl" placeholder="http://www.youtube.com/watch?v=xxxxxxx" type="text"></input>\
							</div>\
							<div class="four columns">\
								<a id="getVideo" class="button" href="#">Get Video</a>\
							</div>\
						</div>\
					</div>\
				</form>\
				</div>\
		</div>';

		$createStory.uploadPicModalElem = $(uploadPicModalHtml);
		$createStory.createStoryModalElem = $(createStoryModalHtml);
		
		bodyElem.append($createStory.uploadPicModalElem);
		bodyElem.append($createStory.createStoryModalElem);

		var custId = TN.utils.getCookie('TNUser');
		// If cookie exists and not a Guest then user already logged in
		if( !!custId && custId !== TN.guestUserName ) {
		}
		// Else show Login section
		else {
			// Show login dialog
			$('.clipthis-loging').show();
			$('#HfamilyTab span, #csStoryHeadlineCounter, #csOpEdCounter').hide();
		}
		
		// if post story mode
		if( !imageUrl ){
			$createStory.createStoryModalElem.find('.clipPostRevealHeader').text('Post a Story');
			$createStory.uploadPicModalElem.find('.clipPostRevealHeader').text('Post a Story');
		}
		else {
			$createStory.createStoryModalElem.find('.clipPostRevealHeader').text('Clip a Story');
			$createStory.uploadPicModalElem.find('.clipPostRevealHeader').text('Clip a Story');
			createImageUrl = imageUrl;
		}
				
		$('.expandedWhereFields').hide();

		// Code to manage character counters:
		var alerted = false;

		$('#HfamilyTab input, #FreefamilyTab textarea, #csHeadlineInput').keyup(function(event){
			var keyupFieldName = event.target.name;
			var counterElem, counterElemValue;

			if ( (keyupFieldName == 'OpEdWhere') || (keyupFieldName == 'Where') || (keyupFieldName == 'address') 
				|| (keyupFieldName == 'stateCountry') || (keyupFieldName == 'city') || (keyupFieldName == 'zipcode') ) {
				
				recreateWhereString();
				(opEdTabSelected) ? counterElem = $('#csOpEdWhereCounter') : counterElem = $('#csWhereCounter');
				counterElemValue = parseFloat(counterElem.text());
				counterElem.text(200 - whereFinalString.length);
				if (whereFinalString.length > 200) {
					if (!alerted) {
						TN.utils.passiveNotification('Character limit reached!', "The total number of charaters in location related fields is 200.");
						alerted = true;
					}
					counterElem.css('color', 'red');
				}
				else counterElem.css('color', '#CCCCCC');			
			}

			else if (keyupFieldName == 'OpEd') {
				counterElem = $('.tabs-content').find('#cs' + keyupFieldName + 'Counter');
				counterElemValue = parseFloat(counterElem.text());
				counterElem.text(1000 - event.target.value.length);
				if (event.target.value.length > 1000) {
					if (!alerted) {
						TN.utils.passiveNotification('Character limit reached!', "The maximum number of charaters for this field is 1000.");
						alerted = true;
					}
					counterElem.css('color', 'red');
				}
				else counterElem.css('color', '#CCCCCC');
			}

			else if (keyupFieldName == 'StoryHeadline') {
				counterElem = $('#csStoryHeadlineCounter');
				counterElemValue = parseFloat(counterElem.text());
				counterElem.text(50 - event.target.value.length);
				if (event.target.value.length > 50) {
					if (!alerted) {
						TN.utils.passiveNotification('Character limit reached!', "The maximum number of charaters for this field is 50.");
						alerted = true;
					}
					counterElem.css('color', 'red');
				}
				else counterElem.css('color', '#CCCCCC');
			}

			else {
				counterElem = $('.tabs-content').find('#cs' + keyupFieldName + 'Counter');
				counterElemValue = parseFloat(counterElem.text());
				counterElem.text(100 - event.target.value.length);
				if (event.target.value.length > 100) {
					if (!alerted) {
						TN.utils.passiveNotification('Character limit reached!', "The maximum number of charaters for this field is 100.");
						alerted = true;
					}
					counterElem.css('color', 'red');
				}
				else counterElem.css('color', '#CCCCCC');
			}

			//console.log('event.target.length: ' + event.target.value.length + ' counterElemValue: ' + counterElemValue + ' keyupFieldName: ' + keyupFieldName);
			counterElem.show();
		});

		$('.locationPin, #csOpEdWherePinIcon').click(function(){
			toggleExpandedWhere();
		});

		// for Clip story show the base details
		if( !!imageUrl ){
			$('#csBaseOptions').show();
		}
		
		$('input[placeholder]').placeholder();
				
		$('#csTellYourStory').click(function(){
			$('#csTellYourStoryDetails').show().find('textarea').autosize();
		});
		
		$('#csFacebookBox,#csTwitterBox').click(function(){
			var jqElem = $(this);
			
			if( jqElem.hasClass('csCheckBoxUnSelected') ){
				jqElem.removeClass('csCheckBoxUnSelected').addClass('csCheckBoxSelected');
			} else {
				jqElem.removeClass('csCheckBoxSelected').addClass('csCheckBoxUnSelected');
			}
		});
		
		$('#csFacebookBox+li,#csTwitterBox+li').click(function(){
			$(this).prev().click();
		});

		
        // Functions to copy expanded Where fields to the other tab if any empty equivalents 
        // are found in other tab and refresh Where counter after the copy
        $('#writeOpEdTab').click(function(){
        	opEdTabSelected = true;
        	if (TN.utils.isBlank($('#csOpEdWhereInput').val()) && !TN.utils.isBlank($('#csWhereInput').val())) 
        		$('#csOpEdWhereInput').val($('#csWhereInput').val());
        	if (TN.utils.isBlank($('#csOpEdAddressInput').val()) && !TN.utils.isBlank($('#csAddressInput').val())) 
        		$('#csOpEdAddressInput').val($('#csAddressInput').val());
        	if (TN.utils.isBlank($('#csOpEdCityInput').val()) && !TN.utils.isBlank($('#csCityInput').val())) 
        		$('#csOpEdCityInput').val($('#csCityInput').val());
        	if (TN.utils.isBlank($('#csOpEdStateCountryInput').val()) && !TN.utils.isBlank($('#csStateCountryInput').val())) 
        		$('#csOpEdStateCountryInput').val($('#csStateCountryInput').val());
        	if (TN.utils.isBlank($('#csOpEdZipcodeInput').val()) && !TN.utils.isBlank($('#csZipcodeInput').val())) 
        		$('#csOpEdZipcodeInput').val($('#csZipcodeInput').val());
        	
        	// Refresh respective where counter:
        	recreateWhereString();
			var counterElem = $('#csOpEdWhereCounter');
			var counterElemValue = parseFloat(counterElem.text());
			counterElem.text(200 - whereFinalString.length);
			if (whereFinalString.length > 200) {
				if (!alerted) {
					TN.utils.passiveNotification('Character limit reached!', "The total number of charaters in location related fields is 200.");
					alerted = true;
				}
				counterElem.css('color', 'red');
			}
			else counterElem.css('color', '#CCCCCC');
			if (whereFinalString.length > 0) counterElem.show();
        });

        $('#writeStoryTab').click(function(){
        	opEdTabSelected = false;
        	if (TN.utils.isBlank($('#csWhereInput').val()) && !TN.utils.isBlank($('#csOpEdWhereInput').val())) 
        		$('#csWhereInput').val($('#csOpEdWhereInput').val());
        	if (TN.utils.isBlank($('#csAddressInput').val()) && !TN.utils.isBlank($('#csOpEdAddressInput').val())) 
        		$('#csAddressInput').val($('#csOpEdAddressInput').val());
        	if (TN.utils.isBlank($('#csCityInput').val()) && !TN.utils.isBlank($('#csOpEdCityInput').val())) 
        		$('#csCityInput').val($('#csOpEdCityInput').val());
        	if (TN.utils.isBlank($('#csStateCountryInput').val()) && !TN.utils.isBlank($('#csOpEdStateCountryInput').val())) 
        		$('#csStateCountryInput').val($('#csOpEdStateCountryInput').val());
        	if (TN.utils.isBlank($('#csZipcodeInput').val()) && !TN.utils.isBlank($('#csOpEdZipcodeInput').val())) 
        		$('#csZipcodeInput').val($('#csOpEdZipcodeInput').val());
        	
        	// Refresh respective where counter:
        	recreateWhereString();
			var counterElem = $('#csWhereCounter');
			var counterElemValue = parseFloat(counterElem.text());
			counterElem.text(200 - whereFinalString.length);
			if (whereFinalString.length > 200) {
				if (!alerted) {
					TN.utils.passiveNotification('Character limit reached!', "The total number of charaters in location related fields is 200.");
					alerted = true;
				}
				counterElem.css('color', 'red');
			}
			else counterElem.css('color', '#CCCCCC');
			if (whereFinalString.length > 0) counterElem.show();
        });
        	
        var foundLocations = {}, currentLocations = {}; 
        var opEdTabSelected = false;
        var whereExpanded = false;
        var whereOpEdExpanded = false;
        var selectedLatitude = "";
        var selectedLongitude = "";
        var whereKeyups = 0;
        var whereFinalString = "";
        var tempWhereArr = [];
		var warnedBeforePublish = false;
		
		var createStoryModalOptions = {
			"closed": function(){
				$createStory.createStoryModalElem.empty().remove();				
			}	
		};
		
		var uploadPicModalOptions = {
			"closed": function(){
				$createStory.uploadPicModalElem.empty().remove();				
			}	
		};

		function recreateWhereString() {
			tempWhereArr = [];
			if (opEdTabSelected) $.each([$('#csOpEdWhereInput').val(), $('#csOpEdStateCountryInput').val(), $('#csOpEdCityInput').val(), $('#csOpEdAddressInput').val()], function(i, val) {
					if (!TN.utils.isBlank(val)) tempWhereArr.push(val);
				});
			else $.each([$('#csWhereInput').val(), $('#csStateCountryInput').val(), $('#csCityInput').val(), $('#csAddressInput').val()], function(i, val) {
				if (!TN.utils.isBlank(val)) tempWhereArr.push(val);
			});
			whereFinalString = tempWhereArr.join(', ');
			if (opEdTabSelected && (!TN.utils.isBlank($('#csOpEdZipcodeInput').val()))) whereFinalString += ' ' + $('#csOpEdZipcodeInput').val();
			if (!opEdTabSelected && (!TN.utils.isBlank($('#csZipcodeInput').val()))) whereFinalString += ' ' + $('#csZipcodeInput').val();
		}

		function toggleExpandedWhere() {
			//log('start, whereOpEdExpanded: '+whereOpEdExpanded+' whereExpanded: '+whereExpanded);
			if (opEdTabSelected) {
				if (whereOpEdExpanded) {
					$('.expandedWhereFields').hide('fast');
					$('#csOfferedGeopositions, #csOpEdOfferedGeopositions').show();
					whereOpEdExpanded = false;
				}
				else {
					$('.expandedWhereFields').show('fast');
					$('#csOpEdOfferedGeopositions').hide();
					whereOpEdExpanded = true;
				}
			}
			else {
				if (whereExpanded) {
					$('.expandedWhereFields').hide('fast');
					$('#csOfferedGeopositions, #csOpEdOfferedGeopositions').show();
					whereExpanded = false;
				}
				else {
					$('.expandedWhereFields').show('fast');
					$('#csOfferedGeopositions').hide();
					whereExpanded = true;
				}
			}
		}

        function loadAndAppendGeoOffer(index){
        	var lat = foundLocations[index]["latitude"];
        	var lon = foundLocations[index]["longitude"];
        	var geoUriHtml = '<a\
	        	class="autoGeoposition"\
	        	title="Click to use this auto-detected location"\
	    		onclick="if ( $(this).hasClass(\'selectedGeoPosition\') ){\
	    			selectedLatitude=\'\';\
	    			selectedLongitude=\'\';\
	    			$(this).removeClass(\'selectedGeoPosition\');\
	    		}\
	    		else {\
	    			selectedLatitude=\''+lat+'\';\
	        		selectedLongitude=\''+lon+'\';\
	        		$(\'.autoGeoposition\').removeClass(\'selectedGeoPosition\');\
	        		$(this).addClass(\'selectedGeoPosition\');\
	        	}"\
	        	href="javascript:void(0);"\
	        	name="'+index+'">\
	        	'+index+'\
	        	</a>';
	        $(geoUriHtml).appendTo('#csOfferedGeopositions, #csOpEdOfferedGeopositions');
        }

        function geoUriInCurrentLocations(lat, lon){
        	// Check if given lat lon exists in currentLocations object, return bool
        	$.each(currentLocations, function(i, val){
        		//log( (val.latitude == lat) && (val.longitude == lon) );
        		if ( (val.latitude == lat) && (val.longitude == lon) ) return true;
        	});
        	return false;
        }

        function latLongLookup(txtarr){
            for (i = 0; i<txtarr.length; i++) {

                if (!!foundLocations[txtarr[i]]) {
                	loadAndAppendGeoOffer(txtarr[i]); 
                	continue; 
                }
                
                (function(index) {
	                TN.services.getLatLong(index, "").done(function(json){
	                    if (!!json) {
	                        if ((json[0] != null) && (json[1] != null)) {
	                            foundLocations[index] = {}; currentLocations[index] = {};
	                            foundLocations[index]["latitude"] = json[0]; 
	                            currentLocations[index]["latitude"] = json[0];
	                            foundLocations[index]["longitude"] = json[1]; 
	                            currentLocations[index]["longitude"] = json[1];
	                            if (!(geoUriInCurrentLocations(json[0], json[1]))) loadAndAppendGeoOffer(index); 
	                        }
	                    }

	                });
	            })(txtarr[i]);

            }
        }

        // Returns array of all combinations of unique sequential n-tuples from a string.
        // Currently works for n: 1, 2 & 3
        function extractTextTuples(string, n){

        	function prune_duplicates(arr){
			    var uniques = [];
			    $.each(arr, function(i, val){
			        if($.inArray(val, uniques) === -1) uniques.push(val);
			    });
			    return uniques;
			}

            var testarr = string.split(" ");
            var temp = [];
            jQuery.each(testarr, function(i, val){
                testarr2 = val.split(",");
                jQuery.each(testarr2, function(i, val){
                    if (val != "") temp.push(val);
                });
            });
            if (n==1) return prune_duplicates(temp);
            var subs = [];
            if (n==2) {
                if (temp.length<2) {
                    return [];
                }
                else {
                    for (i=0; i<temp.length-1; i++) {
                        subs.push(temp[i]+" "+temp[i+1]);
                    }
                    return prune_duplicates(subs);
                }
            }
            else if (n==3) {
                if (temp.length<3) {
                    return [];
                }
                else {
                    for (i=0; i<temp.length-2; i++) {
                        subs.push(temp[i]+" "+temp[i+1]+" "+temp[i+2]);
                    }
                    return prune_duplicates(subs);
                }
            }
            else return [];
        }

        function enqueueLatLongLookup(txt, keystrokeN){

            if (keystrokeN == whereKeyups) {
            	currentLocations = {};
            	$('.autoGeoposition').remove();
                var tuples_3 = extractTextTuples(txt, 3);
                var tuples_2 = extractTextTuples(txt, 2);
                var tuples_1 = extractTextTuples(txt, 1);
                if (tuples_3.length>0) { latLongLookup(tuples_1); latLongLookup(tuples_2); latLongLookup(tuples_3) }
                else {
                    if (tuples_2.length>0) { latLongLookup(tuples_1); latLongLookup(tuples_2) }
                    else latLongLookup(tuples_1);
                }
            }
        }

        function extendedWhereFieldsFilled() {
			if (opEdTabSelected) {
				if (!TN.utils.isBlank($('#csOpEdCityInput').val()) || !TN.utils.isBlank($('#csOpEdStateCountryInput').val()) || !TN.utils.isBlank($('#csOpEdAddressInput').val())) return true;
				else return false;
			}
			else {
				if (!TN.utils.isBlank($('#csCityInput').val()) || !TN.utils.isBlank($('#csStateCountryInput').val()) || !TN.utils.isBlank($('#csAddressInput').val())) return true;
				else return false;
			}
		}
        
        function postToFacebook(buzzId){
    		TN.services.getStory(buzzId).done(function(msg){
    			if (!!msg[0]) {
    				var infoStruct = msg[0];
    				if (typeof infoStruct.messageId == "number") {
		                TN.services.getServerAddress().done(function(msg){
		                    var fullyQualifiedImgUrl = 'http://'+msg+ infoStruct.imageThumbUrl;
		                    var postInfo = {
		                    		'url' : fullyQualifiedImgUrl,
		                    		'headline' : infoStruct.headline,
		                    		'messageId' : infoStruct.messageId
		                    };
		                    
		                    TN.FB.post(postInfo, function(response){
		                    	if( !!response ){
		                        	if( !!response.error ){
	                                    _gaq.push(['_trackEvent', 'Story', 'Post to Facebook', 'User: ' + custId + ', Headline: ' + headline]);	                            			
	    			                	TN.utils.passiveNotification('Warning!', 'Story successfully added to TinyNews but there was a problem (' + response.error.message + ') posting it to Facebook.  You can try posting it again to Facebook via the share functionality.');
		                        	}
		                        	else {
		                        		TN.utils.passiveNotification('Success!', "This story has been successfully added to TinyNews and posted on your Facebook wall.");
		                        	}
		                    	}
	            	  			TN.createStory.close();                			  			
		                    });
		                    
		                }).fail(function(){
		                	TN.utils.passiveNotification('Warning!', 'Story successfully added to TinyNews but there was a problem posting it to Facebook.  You can try posting it again to Facebook via the share functionality.');
		    	  			TN.createStory.close();                			  			
		                });
    				} else {
	                	TN.utils.passiveNotification('Warning!', 'Story successfully added to TinyNews but there was a problem posting it to Facebook.  You can try posting it again to Facebook via the share functionality.');
	    	  			TN.createStory.close();    					
    				}
    			} else {
                	TN.utils.passiveNotification('Warning!', 'Story successfully added to TinyNews but there was a problem posting it to Facebook.  You can try posting it again to Facebook via the share functionality.');
    	  			TN.createStory.close();    					
    			}
    		}).fail(function(){
            	TN.utils.passiveNotification('Warning!', 'Story successfully added to TinyNews but there was a problem posting it to Facebook.  You can try posting it again to Facebook via the share functionality.');
	  			TN.createStory.close();                			  			    			
    		});
        }

		// Publish functionality
		$('#publishButton').click(function(){
			if(!!publishInProgress ){
				TN.utils.passiveNotification('Publishing...', 'This story is already being published.  Please wait.');
				return;
			}
						
			var custId = TN.utils.getCookie('TNUser');
			var headline = $.trim($('#csHeadlineInput').val());
			var catType = TN.categories.getValue(catListElem.find('.current').text());
			
			if( !!headline && !!catType && !!custId && (catType != "Choose A Category") ){

				// Generate 'Where' string parameter for sendBuzzRequest (formatted concatenation of all location related fields in *opened* tab)
				recreateWhereString();

				// We issue only 1 warning if fields are filled beyond limit:
				if (headline.length > 50) {
					if (!warnedBeforePublish) {
						TN.utils.passiveNotification("Warning:", "Some text fields are filled beyond character limit. Please shorten them before publishing.");
						warnedBeforePublish = true;
						return;
					}
				}
				else if (whereFinalString.length > 200) {
					if (!warnedBeforePublish) {
						TN.utils.passiveNotification("Warning:", "Some text fields are filled beyond character limit. Please shorten them before publishing.");
						warnedBeforePublish = true;
						return;
					}
				}
				else if ( ( $('#csWhoInput').val().length > 100 ) || 
				 	 ( $('#csWhatInput').val().length > 100 ) || 
				 	 ( $('#csWhenInput').val().length > 100 ) || 
					 ( $('#csHowInput').val().length > 100 ) || 
					 ( $('#csWhyInput').val().length > 100 ) ) {
					if (!warnedBeforePublish) {
						TN.utils.passiveNotification("Warning:", "Some text fields are filled beyond character limit. Please shorten them before publishing.");
						warnedBeforePublish = true;
						return;
					}
				}
				else if ($('#csOpEdInput').val().length > 1000) {
					if (!warnedBeforePublish) {
						TN.utils.passiveNotification("Warning:", "Some text fields are filled beyond character limit. Please shorten them before publishing.");
						warnedBeforePublish = true;
						return;
					}
				}

				// Beyond that one warning we just trim before publishing to fit the sendBuzzRequest limits
				if ( headline.length > 50 ) headline = headline.substring(0,50);
				if ( $('#csWhoInput').val().length > 100 ) $('#csWhoInput').val($('#csWhoInput').val().substring(0,100));
				if ( $('#csWhatInput').val().length > 100 ) $('#csWhatInput').val($('#csWhatInput').val().substring(0,100));
				if ( $('#csWhenInput').val().length > 100 ) $('#csWhenInput').val($('#csWhenInput').val().substring(0,100));
				if ( $('#csHowInput').val().length > 100 ) $('#csHowInput').val($('#csHowInput').val().substring(0,100));
				if ( $('#csWhyInput').val().length > 100 ) $('#csWhyInput').val($('#csWhyInput').val().substring(0,100));
				if ( $('#csOpEdInput').val().length > 1000 ) $('#csOpEdInput').val($('#csOpEdInput').val().substring(0,1000));
				// If our concatenation of location fields exceeds allowed length of 100 characters we trim it as well
				if (whereFinalString.length > 200) whereFinalString = whereFinalString.substring(0,200);

				// If we are defining 'Where' in 'expanded mode' we will not rely on manually selected geoURI
				// but will instead call TN.services.getLatLong just before calling 'sendBuzzRequest' with all
				// values from expanded Where fields and pipe received result to 'sendBuzzRequest'.
				// In case of error with execution or emptiness of all important expanded Where fields,
				// we will use previously defined latitude and longitude, if any
				// note: currently, geoURI automatic location offerings are disabled
				publishInProgress = true;
			  	$('#publishButton').text('Publishing...');			  	
			  	
				if ( extendedWhereFieldsFilled() && ((opEdTabSelected && whereOpEdExpanded) || (!opEdTabSelected && whereExpanded)) ) {
					$.when(TN.services.getLatLong(	( opEdTabSelected ? $('#csOpEdCityInput').val() : $('#csCityInput').val() ), 
						( opEdTabSelected ? $('#csOpEdStateCountryInput').val() : $('#csStateCountryInput').val() ), 
					  	( opEdTabSelected ? $('#csOpEdZipcodeInput').val() : $('#csZipcodeInput').val() ), 
					  	( opEdTabSelected ? $('#csOpEdAddressInput').val() : $('#csAddressInput').val() ))).then(function(data){
						  	if(!!data) if(!!data[0]) {
								selectedLatitude = data[0];
								selectedLongitude = data[1];
							}
							//publishInProgress = true;
							TN.services.sendBuzzRequest( custId, headline, catType, $('#csImage').attr('src'), storyUrl,
								$('#csOpEdInput').val(), $('#csWhoInput').val(), $('#csWhatInput').val(), $('#csWhenInput').val(),
								whereFinalString, $('#csHowInput').val(), $('#csWhyInput').val(),
                                (!!selectedLatitude? selectedLatitude : ''), (!!selectedLongitude? selectedLongitude : ''), $('#videoIdInput').val() ).
                                done ( function(msg){
                                	if ( (msg[0] == "false") || (msg[0] == false) ) TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg);
                            		else if (!!msg[0]) {
                                        _gaq.push(['_trackEvent', 'Story', 'Post', 'User: ' + custId + ', Headline: ' + headline]);	                            			
                            			TN.utils.passiveNotification('Added!', 'Story successfully added to TinyNews.');
                        			  	// publish to Facebook if it is checked
                        			  	if( $('#facebookPost input').is(':checked')){
                        			  		postToFacebook(msg[0]);
                        			  	}
                        			  	else {                        			
                                			TN.createStory.close();
                        			  	}
                            		}
                                }).
                                fail( function (msg){
//	                    			TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg); 
	                    		}).
	                            always( function(){
	                            	publishInProgress = false;
	    						  	$('#publishButton').text('Publish or Perish');
	                            });	
							}, 
							// In case there was error with execution of TN.services.getLatLong
							function(){
								//publishInProgress = true;
								TN.services.sendBuzzRequest( custId, headline, catType, $('#csImage').attr('src'), storyUrl,
									$('#csOpEdInput').val(), $('#csWhoInput').val(), $('#csWhatInput').val(), $('#csWhenInput').val(),
									whereFinalString, $('#csHowInput').val(), $('#csWhyInput').val(),
                                    (!!selectedLatitude? selectedLatitude : ''), (!!selectedLongitude? selectedLongitude : ''), $('#videoIdInput').val() ).
	                                done ( function(msg){
	                                	if ( (msg[0] == "false") || (msg[0] == false) ) TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg);
	                            		else if (!!msg[0]) {
	                                        _gaq.push(['_trackEvent', 'Story', 'Post', 'User: ' + custId + ', Headline: ' + headline]);	                            			
	                            			TN.utils.passiveNotification('Added!', 'Story successfully added to TinyNews.');
	                        			  	// publish to Facebook if it is checked
	                        			  	if( $('#facebookPost input').is(':checked')){
	                        			  		postToFacebook(msg[0]);
	                        			  	}
	                        			  	else {                        			
	                                			TN.createStory.close();
	                        			  	}
	                            		}
	                                }).
	                                fail( function (msg){
//		                    			TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg.responsetext); 
		                    		}).
		                            always( function(){
		                            	publishInProgress = false;
		    						  	$('#publishButton').text('Publish or Perish');
		                            });	
							}
						);
	            }
	            else {
	     			//publishInProgress = true;
	             	TN.services.sendBuzzRequest( custId, headline, catType, $('#csImage').attr('src'), storyUrl,
						$('#csOpEdInput').val(), $('#csWhoInput').val(), $('#csWhatInput').val(), $('#csWhenInput').val(),
						whereFinalString, $('#csHowInput').val(), $('#csWhyInput').val(),
	                    (!!selectedLatitude? selectedLatitude : ''), (!!selectedLongitude? selectedLongitude : ''), $('#videoIdInput').val() ).
                        done ( function(msg){
                        	if ( (msg[0] == "false") || (msg[0] == false) ) TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg);
                    		else if (!!msg[0]) {
                    			
                                _gaq.push(['_trackEvent', 'Story', 'Post', 'User: ' + custId + ', Headline: ' + headline]);	                            			
                    			TN.utils.passiveNotification('Added!', 'Story successfully added to TinyNews.');
                			  	// publish to Facebook if it is checked
                			  	if( $('#facebookPost input').is(':checked')){
                			  		postToFacebook(msg[0]);
                			  	}
                			  	else {                        			
                        			TN.createStory.close();
                			  	}
                    		}
                        }).
                        fail( function (msg){
//                			TN.utils.passiveNotification('Error!', 'A problem occurred: ' + msg); 
                		}).
                        always( function(){
                        	publishInProgress = false;
						  	$('#publishButton').text('Publish or Perish');
                        });	
	            }
			}
			else {
				if( !headline ){
					TN.utils.passiveNotification('No headline written yet!', 'Please enter a headline for this story');
				}
				
				if( !catType || (catType == "Choose A Category")){
					TN.utils.passiveNotification('No category selected yet!', 'Please select a category for this story');
				}
				
				if( !custId ){
					TN.utils.passiveNotification('Session Timed Out!', 'Unable to validate your user Id.  Your session may have timed out.  Please sign in again.');
					TN.createStory.close();
				}
			}
		});
		
		// login button functionality
		$('.clipthis-login_submitBox').click(function(){
			
			var pass = $('#csPassword').val();
			
			//source to check if valid email is entered
			//http://www.w3schools.com/js/js_form_validation.asp
			var userName = $('#csUsername').val();
			var email='', uniqueid='';
			
			var atpos=userName.indexOf("@");
			var dotpos=userName.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=userName.length){
				uniqueid = userName;
			} else{
				email = userName;
			}
			
			TN.services.loginCustomer(uniqueid, email, pass).done(function(){
				TN.utils.setCookie('TNUser', userName, null, 345600);
				//$('#loginCont').remove();
				//$('#loginCanvas').remove();

				$('.clipthis-loging').fadeOut('slow', function(){
					// It's safe to show absolutely positioned counters after login form hides:
					loadCatsDropDown();
					$('#HfamilyTab span, #csStoryHeadlineCounter, #csOpEdCounter').show();
				});
				
				//$('#csTitle').removeClass('displayNone');
			}).fail(function(jqXHR, textStatus){
				if( jqXHR.status === 404 ){
					TN.utils.passiveNotification('Wrong credentials.', 'Please try again.');
				}
			});	
		});
		
		// Video functionality
		$('#getVideo').click(function(){
			var videoUrl = $('#videoUrl').val();
			var youtubeIdParm = videoUrl.substr(videoUrl.indexOf('v='));
			if( !!youtubeIdParm && videoUrl.indexOf('youtube.com') >= 0 ){
				var youtubeId = youtubeIdParm.split('=')[1];
				var youtubeImageUrl = 'http://img.youtube.com/vi/' + youtubeId + '/hqdefault.jpg';
				$('#videoIdInput').val(youtubeId);
				$('#csImage').attr('src', youtubeImageUrl);
				$('#videoCont').attr('src', 'http://www.youtube.com/v/' + youtubeId);
				$('#videoCont').show();
				$('#csEditPhoto').hide();
				$createStory.createStoryModalElem.reveal(createStoryModalOptions);
			}
			else {
				TN.utils.passiveNotification('Invalid Youtube URL!', "Please enter a valid Youtube video URL.");
				$('#videoUrl').focus();
			}
		});
		
		// Choose photo functionality
		var choosePhotoFormOptions = {
			success: function(responseText, statusText, xhr, $form){
				$('#csImage').attr('src', responseText);
				//$('#csBrowseFileCont').hide();
				//$('#csBaseOptions').show();
				$('#videoCont').hide();
				$('#csEditPhoto').show();
				$('#csChoosePhotoInputFake').val('Browse');
				$createStory.createStoryModalElem.reveal(createStoryModalOptions);
			},
			url: "/php/store_image.php",
			dataType:'text'
		};
		
		if( $.browser.mozilla && (parseInt($.browser.version, 10) < 22) ){
			$('#csChoosePhotoInputFakeLabel').click(function(){
				$('#csChoosePhotoInputFake').unbind('click');
				$('#csChoosePhotoInput').click();
			});
		}
		
		$('#csChoosePhotoInput').change(function(){
			$('#csChoosePhotoInputFake').val('Loading image...');
			$('#csChoosePhotoForm').ajaxSubmit(choosePhotoFormOptions);
		});
		
		// Do not allow edit photo functionality in Clip This scenario
		if( !imageUrl ){
			// Edit photo functionality
			var editPhotoFormOptions = {
				success: function(responseText, statusText, xhr, $form){
					$('#csImage').attr('src', responseText);
					//
				},
				url: "/php/store_image.php",
				dataType:'text'
			};
			
			if( $.browser.mozilla && (parseInt($.browser.version, 10) < 22) ){
				$('#csImage').click(function(){
					$('#csEditPhotoInput').click();
				});
			}
					
			$('#csEditPhotoInput').change(function(){
				$('#csFoundationWrapperForm').ajaxSubmit(editPhotoFormOptions);
			});
		}
				
		//$('#createStory .csCloseButton, #createStoryCanvas, #cancelLink').click($createStory.close);
		$('.close-reveal-modal').click($createStory.close);

		
		var csCatDropdown = $('#customDropdown > ul');
        var catListElem = $('#customDropdown');

        function addCategoryItem(itemText){
        	var categoryItemElem = null;
        	
           	categoryItemElem = $('<li>' + itemText + '</li>');
        	
        	categoryItemElem.click(function(){
        		var currElem = $(this);
        		//console.log(catListElem, currElem.text());
                TN.utils.setDropdownValue(catListElem, currElem.text());
        		$('body').click();
        		return false;
        	});
        	
        	csCatDropdown.append(categoryItemElem);
        }
       	
       	//addCategoryItem('');
        TN.utils.setDropdownValue(catListElem, 'Choose A Category');

        function loadCatsDropDown(){
        	// Category drop down functionality
			TN.services.getAllMessageTypeCategories().done(function(response){
				var json;
				// workaround for IE where sometimes response comes as a string instead of array
				if( typeof response == 'string' ){
					json = $.parseJSON(response);
				}
				else {
					json = response;
				}
				
				if( !!json ){
					var maxItems = json.length;
					for( var i = 0; i < maxItems; i++ ){
						if (json[i].id == "Q") continue;
						if( json[i].global === 1 || json[i].global === 0 ){
							var currCat = json[i].id;
							var currCatLabel = TN.categories.getLabel(currCat);
							if( !!currCatLabel ){
								addCategoryItem(currCatLabel);
							}
						}
					}

				}

			}).fail(function(){
				TN.utils.passiveNotification('Error!', 'Unable to load the available categories due to a system error.  Please try again later.');
			});
        }

		custId = TN.utils.getCookie('TNUser');
		// If cookie exists and not a Guest then user already logged in
		if( !!custId && custId !== TN.guestUserName ) {
			loadCatsDropDown();
        }		

		//Write Story tabs functionality
		$('#writeOpEdInput').hide();
		
		$('#writeStoryTab').click(function(){
			var jqElem = $(this);
			jqElem.addClass('tabSelected');
			$('#writeStoryInputs').show();
			$('#writeOpEdTab').removeClass('tabSelected');
			$('#writeOpEdInput').hide();			
		});
		
		$('#writeOpEdTab').click(function(){
			var jqElem = $(this);
			jqElem.addClass('tabSelected');
			$('#writeOpEdInput').show();
			$('#writeStoryTab').removeClass('tabSelected');
			$('#writeStoryInputs').hide();			
		});
		
		if (!!imageUrl) {
			$createStory.createStoryModalElem.find('#csImage').attr('src', imageUrl); 
			$('#csEditPhoto').show();
			$createStory.createStoryModalElem.reveal(createStoryModalOptions);	
		}
		else $createStory.uploadPicModalElem.reveal(uploadPicModalOptions);

	}
	
	$createStory.show = function(imageUrl, storyUrl, resetAddressBar){
				
		showCreateStory(imageUrl, storyUrl, resetAddressBar, true);

	};
}(TN.createStory));
