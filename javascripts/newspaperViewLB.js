if (!TN) var TN= {};
if(!TN.newspaperViewLB) TN.newspaperViewLB = {};

(function($newspaperViewLB){
	
	var custId = TN.utils.getCookie('TNUser');
    var currNewspaperId = 0;
    var temp;
        
    function getStoryHtml(storyObj){
		
		function getOpedHtml(){
			var opedHtml = "";
			
			if( !!storyObj.buzzWho ){
				opedHtml += '<p><span class="fiveWAndH">Who</span>: '+ storyObj.buzzWho + '</p>';
			}
			if( !!storyObj.buzzWhat ){
				opedHtml += '<p><span class="fiveWAndH">What</span>: '+ storyObj.buzzWhat + '</p>';
			}
			if( !!storyObj.buzzWhere ){
				opedHtml += '<p><span class="fiveWAndH">Where</span>: '+ storyObj.buzzWhere + '</p>';
			}
			if( !!storyObj.buzzWhen ){
				opedHtml += '<p><span class="fiveWAndH">When</span>: '+ storyObj.buzzWhen + '</p>';
			}
			if( !!storyObj.buzzHow ){
				opedHtml += '<p><span class="fiveWAndH">How</span>: '+ storyObj.buzzHow + '</p>';
			}
			if( !!storyObj.buzzWhy ){
				opedHtml += '<p><span class="fiveWAndH">Why</span>: '+ storyObj.buzzWhy + '</p>';
			}
			if( !!storyObj.buzzOped ){
				opedHtml += '<p>'+ storyObj.buzzOped + '</p>';
			}
			
			return opedHtml;
		}
		
		var storyHtml = '<div id="story_' + storyObj.buzzId + '" class="storyNPBlock" story-id="' + storyObj.buzzId +'">\
				<div class="story-img-wrapper">\
					<img class="story-img" style="width:116px;margin-left:5px;margin-right:5px;margin-top:5px;" src="' + storyObj.storyOriginalImg + '" title="Story Image" >\
				</div>\
				<h1 class="storyHeading">' + storyObj.headline + '</h1>\
				<div class="story-content-wrapper">\
	    			<div class="story-content block">\
						<!--input class="storyOriginalImage" type="hidden" value="' + storyObj.storyOriginalImg + '"/>\
						<input class="storyThumbImage" type="hidden" value="' + storyObj.storyThumbImg + '"/>\
						<input class="buzzId" type="hidden" value="' + storyObj.buzzId + '"/> \
						<input class="imageId" type="hidden" value="' + storyObj.imageId + '"/> \
						<input class="elementId" type="hidden" value="' + storyObj.elementId + '"/-->' +
						getOpedHtml() +
	    			'</div>\
	    			<div class="NPLBoriginator-content block">\
	    				<img  src="' + storyObj.originatorImageUrl + '">\
	    				<h1>' + storyObj.originatorName + '</h1>\
	    				<p class="storyDate">' + storyObj.elapsedTime + '</p>\
	    			</div>\
    			</div>\
    		</div>';
		
		return storyHtml;
	}
	
	function getDividerHtml(dividerObj){
		var dividerHtml = '<div class="NPLBdividerHeading">\
								<p>' + dividerObj.headline + '</p> \
						   </div>';
		return dividerHtml;
	}
	
	function loadNewspaper(npId){
		function showNewspaper(json){
			
			if( !!json && !!json[0] ){

				/*var NPTemplateHeader = '<div class="block newspaper-header">\
				    <div class="newspaper-title"><h1><span class="inactive" id="npHeadline"></span></h1></div>\
				    <p class="newspaperSection"><span class="inactive" id="npEdition"></span></p>\
				    <p class="newspaperPlace"><span class="inactive" id="npLocation"></span></p>\
			    </div>';*/

				var NPTemplate = '<div id="NPLBcontentRight" class="NPLBcontentRight block reveal-modal">\
					<a class="close-reveal-modal"></a>\
					<div style="padding:0px 15px 70px 5px; margin-bottom: 40px">\
					    <div class="block NPLBnewspaper-header">\
						    <div class="NPLBnewspaper-title"><h1><span class="inactive" id="npHeadline"></span></h1></div>\
						    <p class="NPLBnewspaperSection"><span class="inactive" id="npEdition"></span></p>\
						    <p class="NPLBnewspaperPlace"><span class="inactive" id="npLocation"></span></p>\
					    </div>\
					    <div id="NPRevealShareButtons"></div>\
					    <div id="NPRevealLikeButtons"></div>\
					    <div id="NPRevealDislikeButtons"></div>\
					    <!--div id="NPRevealCommentButtons"></div-->\
					    <br><br><br>\
						<!-- NewsPaper Layout -->\
						<div class="NPLBnewspaperLayout NPLBlayoutBackground" id="newspaperLayout">\
							<div class="gridster ready">\
							<ul id="gridsterList" style="position: relative;"></ul>\
							</div>\
						</div>\
						<div id="bottomButtonSet" style="float:right">\
							<img style="height:38px" id="NPLBReturnButton" src="images/newspaper/NPLBReturnButton.png" onclick="location.href=\'#NPLBcontentRight\'">\
							<img style="height:38px" id="NPLBBackButton" src="images/newspaper/NPLBBackButton.png" onclick="$(\'a.close-reveal-modal\').trigger(\'click\');">\
						</div>\
					</div>\
				</div>';
				//var NPTemplateHeader = '<div></div>';
				//var NPTemplate = '<div></div>';

				var jQNPTemplate = $(NPTemplate);
				//var jQNPTemplateHeader = $(NPTemplateHeader);

				var newspaperJson = json[0];

				// needed for determining pageFull div height and width:
				var maxYPlusHeight = 0;
				var maxXPlusWidth = 0;
				var minimalYPosition = 99999;

				var isOldFormatNP = false;

				var npHeadlineElem = jQNPTemplate.find('#npHeadline');
				var npEditionElem = jQNPTemplate.find('#npEdition');
				var npLocationElem = jQNPTemplate.find('#npLocation');
				
				var firstImageUrl;
				
				npHeadlineElem.text((!!newspaperJson.headline ? newspaperJson.headline : ''));
				npEditionElem.text((!!newspaperJson.edition ? newspaperJson.edition : ''));
				npLocationElem.text((!!newspaperJson.location ? newspaperJson.location : ''));
				
				jQNPTemplate.find('#NPRevealShareButtons').click(function(){
					if( TN.header.isGuestUser() ) return;
					TN.sharingHandler.initNPShare(npId, newspaperJson.headline, firstImageUrl, function(shareType){
	            		_gaq.push(['_trackEvent', 'Newspaper', 'Share: ' + shareType + ', Headline: ' + newspaperJson.headline, 'User: ' + custId]);            			
	            	});
				});
				
				currNewspaperId = ( !!newspaperJson.id ? newspaperJson.id : 0 );
				
				npHeadlineElem.css('color', newspaperJson.titleFontColor);
				npHeadlineElem.css('font-family', newspaperJson.titleFontStyle);				
				npHeadlineElem.css('font-size', newspaperJson.titleFontSize);
			
				npLocationElem.css('color', newspaperJson.locEdFontColor);
				npEditionElem.css('color', newspaperJson.locEdFontColor);
			
				npLocationElem.css('font-family', newspaperJson.locEdFontStyle);
				npEditionElem.css('font-family', newspaperJson.locEdFontStyle);
			
				npLocationElem.css('font-size', newspaperJson.locEdFontSize);
				npEditionElem.css('font-size', newspaperJson.locEdFontSize);			

				currNewspaperId = ( !!newspaperJson.id ? newspaperJson.id : 0 );
				
				if( !!newspaperJson.dividers ){
					var dividersArray = newspaperJson.dividers;
					var numDividers = dividersArray.length;
					for( var i = 0; i < numDividers; i++ ){
						var currDivider = dividersArray[i];
						var dividerHtml = getDividerHtml(currDivider);
						var dividerHtmlElem = $(dividerHtml);
						
						var coordinates = {
							ypos: currDivider.ypos,
							xpos: currDivider.xpos,
							width: currDivider.pixelWidth,
							height: currDivider.pixelHeight
						};

						if ( !coordinates.ypos && !coordinates.xpos && !coordinates.width && !coordinates.height ) isOldFormatNP = true;

						var newWidget = $('<div></div>').append(dividerHtmlElem);
						
						var headingElem = newWidget.find('.NPLBdividerHeading > p');
						headingElem.css('color', currDivider.fontcolor);
						headingElem.css('font-size', currDivider.fontsize);
						headingElem.css('font-family', currDivider.fontstyle);
						headingElem.css('left', coordinates.xpos+'px').css('top', coordinates.ypos+'px');
						headingElem.css('width', coordinates.width+'px');
						headingElem.css('height', coordinates.height+'px');

						var addedWidget = newWidget;
            			//addedWidget.css('position', 'absolute');           			
        				addedWidget.find('.NPLBdividerHeading').css('height',  addedWidget.height() );

        				jQNPTemplate.find('#gridsterList').append(addedWidget);
					}
				}
				
				if( !!newspaperJson.stories ){
					var storiesArray = newspaperJson.stories;
					var numStories = storiesArray.length;
					for( var i = 0; i < numStories; i++ ){
						var currStory = storiesArray[i];
						var storyHtml = getStoryHtml({
                            headline: currStory.headline,
                            buzzId: currStory.customerBuzzId,
                            imageId:-1,
							elementId:currStory.elementId,
                            storyThumbImg: currStory.buzzThumbImageUrl,
                            storyOriginalImg: currStory.buzzImageUrl,
                            buzzWhat:currStory.what,
                            buzzWhen:currStory.when,
                            buzzWhere:currStory.where,
                            buzzWhy:currStory.why,
                            buzzWho:currStory.who,
                            buzzHow:currStory.how,
                            buzzOped:currStory.oped,
                            originatorImageUrl: currStory.authorImageUrl,
                            originatorName: currStory.authorName,
                            elapsedTime: !!currStory.date ? currStory.date : ''									
						});
											
						if( i===0 ){
							firstImageUrl = currStory.buzzImageUrl;
						}
						
						if ( !currStory.ypos && !currStory.xpos && !currStory.pixelWidth && !currStory.pixelHeight ) isOldFormatNP = true;

						var currHeightTest = currStory.ypos + currStory.pixelHeight;
						if (currHeightTest > maxYPlusHeight) maxYPlusHeight = currHeightTest;
						var currWidthTest = currStory.xpos + currStory.pixelWidth;
						if (currWidthTest > maxXPlusWidth) maxXPlusWidth = currWidthTest;
						
						var currMinYTest = currStory.ypos;
						if (currMinYTest < minimalYPosition) minimalYPosition = currMinYTest;
						
						// Apply CSS tweaks common to both newspaper types
						var storyHtmlElem = $(storyHtml);
						storyHtmlElem.css('opacity', '1.0');
						storyHtmlElem.css('display', 'block');

						var headingElem = storyHtmlElem.find('.storyHeading');
						headingElem.css('color', currStory.fontcolor);
						headingElem.css('font-size', currStory.fontsize);
						headingElem.css('font-family', currStory.fontstyle);
						
						storyHtmlElem.find('.story-content p').each(function(){
							var currOpedElem = $(this);
							currOpedElem.css('color', currStory.bodyfontcolor);
							currOpedElem.css('font-size', currStory.bodyfontsize);
							currOpedElem.css('font-family', currStory.bodyfontstyle);								
						});
						
						storyHtmlElem.find('.story-img').css('width', (currStory.pixelWidth-10)+'px');
						storyHtmlElem.css('width', currStory.pixelWidth+'px');
						storyHtmlElem.css('height', currStory.pixelHeight+'px').css('float', 'left');
						storyHtmlElem.css('position', 'absolute').css('left', currStory.xpos+'px').css('top', currStory.ypos+'px');

						// storyHtmlElem.append(currHeightTest);
						// console.log('currHeightTest: ' + currHeightTest);
						jQNPTemplate.find('#gridsterList').append(storyHtmlElem);
						jQNPTemplate.height(maxYPlusHeight+290);
						jQNPTemplate.find('#bottomButtonSet').css('margin-top', maxYPlusHeight+95);
					}
				}

				if (isOldFormatNP) TN.utils.passiveNotification('Warning!', 'For testers: this is old format newspaper and it doesn\'t have story coordinates saved in proper format.\nPlease resave it in Workspace and try viewing it again.');
				else {
					// Remove previous "reval buffer" before appending new one:
					$('#NPLBcontentRight').remove();
					// Current indicators used to detect beginner level Make Newspaper are either: 
					// - greatest story horizontal point > 779
					// - minimal Y story coordinate greater than 107.
					if ((maxXPlusWidth > 779) || (minimalYPosition > 107)) {
						// console.log('maxYPlusHeight : ' + maxYPlusHeight);
						jQNPTemplate.width(915);
						// IE, Chrome interpret things differently so make an override in that case
						if (typeof navigator.vendor != "undefined") {
							if (navigator.vendor == "Google Inc."){
								jQNPTemplate.width(960);
							}
						};
						if (typeof navigator.appName != "undefined") {
							if (navigator.appName == "Microsoft Internet Explorer") {
								jQNPTemplate.width(960);
							}	
						};
						jQNPTemplate.css('margin-left', '-503px');
						jQNPTemplate.find('.fiveWAndH').css('font-weight', 'bold');
						jQNPTemplate.find('.storyNPBlock').each(function(){
							// Emulate margin-bottom 25px & margin-right 25px applied to story blocks on beginner level 
							// newspaper since margins have no effect on absolutely positioned story blocks
							$(this).css('padding', '9px 17px 19px 17px');
							// Compensate for downward-drifted story blocks by transforming each 145px upwards
							var originalTop = parseFloat($(this).css('top'));
							$(this).css('top', (originalTop-145)+'px');
							// Emulate class and look of .storyByline from news-paper.css:
							var originatorBlock = $(this).find('.NPLBoriginator-content');
							var author = $(originatorBlock).find('h1').text();
							$(originatorBlock).text('by ' + author);
							$(originatorBlock).css('font-size', '12px').css('margin', '0').css('padding-right', '15px').css('padding-top', '15px').css('line-height', '1');
							// Delete all image margins (effectively left, right & top):
							$(this).find('.story-img').css('margin', '0');
							$(this).find('.story-content-wrapper').css('padding-top', '15px');
							$(this).find('.story-content-wrapper p').css('line-height', '22px');
							// Left aligned story headline is also specific to beginner level newspapers
							$(this).find('.storyHeading').css('margin', '0').css('padding-top', '9px').css('padding-bottom', '9px').css('text-align', 'left');
						});
					}
					// Style for expert level newspapers
					else {
						jQNPTemplate.width(779);
					}

					// Style modification common for both kinds of newspaper:
					// Fix button positions in IE, Chrome
					if (typeof navigator.appName != "undefined") {
						if (navigator.appName == "Microsoft Internet Explorer") {
							temp = parseFloat(jQNPTemplate.find('#bottomButtonSet').css('margin-top'));
							jQNPTemplate.find('#bottomButtonSet').css('margin-top', (temp-40)+'px');
						}
					};
					if (typeof navigator.vendor != "undefined") {
						if (navigator.vendor == "Google Inc."){
							temp = parseFloat(jQNPTemplate.find('#bottomButtonSet').css('margin-top'));
							jQNPTemplate.find('#bottomButtonSet').css('margin-top', (temp-40)+'px');
						}
					};

					jQNPTemplate.find('#NPRevealShareButtons, #NPRevealLikeButtons, #NPRevealDislikeButtons, #NPRevealCommentButtons').hover(
						function(){
						    $(this).css('background-position', 'left center');
						},
						function(){
						    if (! $(this).hasClass('npRevealActiveButton') ) $(this).css('background-position', 'right center');
						}
					);

					jQNPTemplate.find('#NPRevealLikeButtons').click(function(event){
						if( TN.header.isGuestUser() ) return;

						TN.services.npLike(custId, currNewspaperId).done(function(msg){
							if (msg[0] == true) {
						        _gaq.push(['_trackEvent', 'Newspaper', 'Like, Newspaper ID: ' + currNewspaperId, 'User: ' + custId]);			
								$('#'+event.currentTarget.id).addClass('npRevealActiveButton');
								$('#NPRevealDislikeButtons').removeClass('npRevealActiveButton');
							}
						});
					});

					jQNPTemplate.find('#NPRevealDislikeButtons').click(function(event){
						if( TN.header.isGuestUser() ) return;

						TN.services.npUnlike(custId, currNewspaperId).done(function(msg){
							if (msg[0] == true) {
						        _gaq.push(['_trackEvent', 'Newspaper', 'Dislike, Newspaper ID: ' + currNewspaperId, 'User: ' + custId]);			
								$('#'+event.currentTarget.id).addClass('npRevealActiveButton');
								$('#NPRevealLikeButtons').removeClass('npRevealActiveButton');
							}
						});
					});

					jQNPTemplate.find('#NPRevealCommentButtons').click(function(){
						if( TN.header.isGuestUser() ) return;

						jQuery.noop();
					});

					$('body').append(jQNPTemplate);
					$('#NPLBcontentRight').reveal();
					skimlinks();
					TN.services.npRead(custId, currNewspaperId);
				}
			}
		}
		
		if( npId > 0 ){
			TN.services.getNewspaper(npId).
				done(showNewspaper).
				fail(function(jqXHR, textStatus, errorThrown){
					TN.utils.passiveNotification('Error!', 'There was an error in retrieving the newspaper: (' +  jqXHR.status + ') ' + errorThrown);
				});
		}
		else {
			TN.utils.passiveNotification('Warning!', 'Invalid newspaper id');
		}
	}
    
	$newspaperViewLB.init = function(npid){
		loadNewspaper(npid);

	};


}(TN.newspaperViewLB));