<?php
  
  // error_reporting(~0);
  // ini_set('display_errors', 1);
  //
  // http://www.eventfinda.com/api/v2/overview

  if ( isset($_GET['lat']) ) {
    $lat = $_GET['lat'];
  }
  else exit('Please provide latitude via lat parameter.');

  if ( isset($_GET['lng']) ) {
    $lng = $_GET['lng'];
  }
  else exit('Please provide longitude via lng parameter.');

  if ( isset($_GET['category_slug']) ) {
    $cSlug = $_GET['category_slug'];
  }
  else $cSlug = '';

  // Providing distance=[km] incapacitates eventfinda API as of 2013-07-20
  // if ( isset($_GET['dst']) ) {
  //   $dst = $_GET['dst'];
  // }
  // else exit('Please provide distance[km] via dst parameter.');

  $url = "http://api.eventfinda.com/v2/events.json?point=$lat,$lng&order=distance&rows=20&category_slug=$cSlug";

  $username = 'tinynewsme';
  $password = 'hvh3gpgs743b';

  // $collection = json_decode($return);

  $apiCacheFile = 'apicaches/eventfinda/'.$lat.'_'.$lng.'_'.$cSlug.'.json';

  if (file_exists($apiCacheFile)) {
      if ( (time() - filemtime($apiCacheFile)) > 21600) {
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
        file_put_contents($apiCacheFile, $return);
    }
      else $return = file_get_contents($apiCacheFile);
  }
  else {
      $process = curl_init($url);
      curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
      curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
      $return = curl_exec($process);
      file_put_contents($apiCacheFile, $return);
  }

  header("Content-Type: application/json; charset=utf-8");
  print ( $return );

?>