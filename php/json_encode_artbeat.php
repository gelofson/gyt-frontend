<?php

	if ( isset($_GET['city']) ) {
		$city = $_GET['city'];
	}
	else exit('Please provide city name via city parameter.');

	if ( isset($_GET['xmlName']) ) {
		$xmlName = $_GET['xmlName'];
	}
	else exit('Please provide list name via xmlName parameter.');

	if ( !preg_match('/^[a-zA-Z0-9_.]{0,200}$/', $xmlName) ) exit('Illegal xmlName parameter provided.');
	if ( !preg_match('/^[a-zA-Z]{0,200}$/', $city) ) exit('Illegal city parameter provided.');

	// http://lostechies.com/seanbiefeld/2011/10/21/simple-xml-to-json-with-php/
	class XmlToJson {
		public function Parse ($url) {
			$fileContents = file_get_contents($url);
			$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
			$fileContents = trim(str_replace('"', "'", $fileContents));
			$simpleXml = simplexml_load_string($fileContents, 'SimpleXMLElement', LIBXML_NOCDATA); // LIBXML_NOCDATA option added
			$json = json_encode($simpleXml);
			return $json;
		}
	}

	// If a cache file exists and is not older than 6 hours, use it. Otherwise create one.
	
	// Try to create dir if it doesn't exist. Doesn't work well & breaks functionality, so commented out.
	/*$apiCacheFileLocation = './apicaches/'.$city.'artbeat';
	if (!file_exists($apiCacheFileLocation)) {
	    mkdir($apiCacheFileLocation, 0777, true);
	}*/

	$apiCacheFile = 'apicaches/'.$city.'artbeat/'.$xmlName.'.json';

	if (file_exists($apiCacheFile)) {
	    if ( (time() - filemtime($apiCacheFile)) > 21600) {
			$parsed = XmlToJson::Parse("http://www.".$city."artbeat.com/list/".$xmlName.".xml");
			file_put_contents($apiCacheFile, $parsed);
		}
	    else $parsed = file_get_contents($apiCacheFile);
	}
	else {
    	$parsed = XmlToJson::Parse("http://www.".$city."artbeat.com/list/".$xmlName.".xml");
    	file_put_contents($apiCacheFile, $parsed);
	}

	header("Content-Type: application/json; charset=utf-8");
	print ( $parsed );

?>