(function() {

Ember.TEMPLATES["templates/newspaper"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data
/**/) {
this.compilerInfo = [3,'>= 1.0.0-rc.4'];
helpers = helpers || Ember.Handlebars.helpers; data = data || {};
  var buffer = '', stack1, hashContexts, hashTypes, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n					");
  hashContexts = {'valueBinding': depth0};
  hashTypes = {'valueBinding': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "TNE.StoryView", {hash:{
    'valueBinding': ("value")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n				");
  return buffer;
  }

  data.buffer.push("<div class=\"row\">\n	<!--describe how to create a story-->\n	<!--div class=\"twelve columns topInstructions\">\n		<h3 class=\"instructions centered\">Choose a Template, Drag your stories, and pick a Theme!</h3>\n	</div-->\n\n	<!--Utilities-->\n	<div class=\"container_16 clearfix\">		\n		<div class=\"navBarGrid\">\n			<ul class=\"nav-bar \">\n				<li class=\"RoundedLeft\">\n					<div class=\"open\">Open</div>\n					");
  hashContexts = {'class': depth0,'contentBinding': depth0,'valueBinding': depth0,'optionLabelPath': depth0,'optionValuePath': depth0};
  hashTypes = {'class': "STRING",'contentBinding': "STRING",'valueBinding': "STRING",'optionLabelPath': "STRING",'optionValuePath': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.Select", {hash:{
    'class': ("allNewspapers"),
    'contentBinding': ("controller.allNewspapers"),
    'valueBinding': ("controller.selectedNewspaperId"),
    'optionLabelPath': ("content.headline"),
    'optionValuePath': ("content.id")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n				</li>\n				<li ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "templatesAction", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"\">Templates</li>\n				<li ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "themesAction", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"\">Themes</li>\n				<li ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "storiesAction", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"\">Stories</li>\n				<li ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "saveNewspaper", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"\">Save</li>\n				<li ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "publishNewspaper", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"RoundedRight\">Publish</li>\n			</ul>\n		</div>\n	</div>\n	<div class=\"container_16 clearfix stories\">\n		<!--stories here-->\n		<div class=\"grid_16 topSection\">\n			<div class=\"leftButtons\">\n				<a ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "allStories", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"typeButton\" id=\"allStories\">All</a>\n				<a ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "mineStories", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"typeButton\" id=\"mine\">Mine</a>\n			</div>\n\n            <div class=\"searchInput\">\n               	<input type=\"text\" value=\"\" class=\"text-box\" id=\"storiesSearch\">\n            </div>\n                   \n            <div class=\"catSelect\">\n				<form class=\"custom\">\n	       			<div class=\"left\">\n	      				<div class=\"custom dropdown\" id=\"storyCatsDropDown\">\n	      					<a href=\"#\" class=\"current\"></a>\n	      					<a href=\"#\" class=\"selector\"></a>\n	      					<ul>\n	      					</ul>\n	      				</div>\n	      			</div>\n				 </form>	                \n            </div>\n		</div>\n\n		<ul id=\"draggable\" class=\"storiesContainer grid_16\">\n			<div id=\"stories\">\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "value", "in", "controller.allOrMineStories", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			</div>\n			<div id=\"themes\" class=\"themes\">\n				<div class=\"themeIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTheme", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"theme\" id=\"Helvetica\" src=\"images/helvetica.png\">\n					<p>Helvetica</p>\n				</div>\n\n				<div class=\"themeIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTheme", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"theme\" id=\"Hipster\" src=\"images/hipster.png\">\n					<p>Hipster</p>\n				</div>\n\n				<div class=\"themeIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTheme", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"theme\" id=\"Hard News\" src=\"images/hard-news.png\">\n					<p>Hard News</p>\n				</div>\n\n				<div class=\"themeIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTheme", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"theme\" id=\"Sporty\" src=\"images/striped.png\">\n					<p>Sporty</p>\n				</div>\n			</div>\n\n			<div id=\"templates\" class=\"themes\">\n				<div class=\"templateIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTemplate", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"template\" id=\"template1\" src=\"images/template-1.png\">\n				</div>\n				<div class=\"templateIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTemplate", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"fadeTemplate\" id=\"template2\" src=\"images/template-2.png\">\n					<p class=\"fadeTemplateText\">Coming Soon</p>\n				</div>\n				<div class=\"templateIconHolder grid_3\">\n					<img ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "applyTemplate", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"fadeTemplate\" id=\"template3\" src=\"images/template-3.png\">\n					<p class=\"fadeTemplateText\">Coming Soon</p>\n				</div>\n			</div>\n\n		</ul>		\n	</div>\n	<div class=\"twelve columns scroll\">\n			<a ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "expandContainer", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"centerd-btn\">\n				<img src=\"images/scroll.png\" alt=\"scroll\">\n			</a>\n	</div>\n</div>\n<!--newspaper creation section-->\n\n<div class=\"row\">\n	<div class=\"twelve columns\">\n		<div class=\"nine columns centered \">\n			<div id=\"newspaperContainer\">\n				<div id=\"newspaper\" class=\"twelve columns stories newspaper\">			\n					<div class=\"twelve columns\">\n						<div class=\"preview\" ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "previewStory", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n							<img src=\"images/preview.png\" alt=\"preview-button\">\n						</div>\n						<div class=\"ten columns middle newspaperTitle editableText\">\n							<h2 ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "editText", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" id=\"npHeadline\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "controller.defaultHeadline", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</h2>\n						</div>						\n\n					<div class=\"twelve columns editableText\">\n						<div class=\"volume left\"><p ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "editText", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"editableText\" id=\"npEdition\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "controller.defaultEdition", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p></div>\n						<div class=\"place right\"><p ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "editText", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"editableText\" id=\"npLocation\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "controller.defaultLocation", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p></div>\n					</div>\n					\n				</div>\n\n					<div class=\"gridster\">\n					    <ul id=\"gridsterList\" class=\"droppable container_16 clearfix\">			        \n				        	<div class=\"grid_16 firstRow\">\n				        		<li id=\"1\" class=\"storyTile\" data-row=\"1\" data-col=\"1\" data-sizex=\"5\" data-sizey=\"4\"><div class=\"tileImage\"><img src=\"images/story-1.png\" alt=\"story\"></div></li>\n						        <li id=\"2\" class=\"storyTile\" data-row=\"2\" data-col=\"1\" data-sizex=\"3\" data-sizey=\"2\"><div class=\"tileImage\"><img src=\"images/story-2.png\" alt=\"story\"></div></li>\n						        <li id=\"3\" class=\"storyTile\" data-row=\"3\" data-col=\"1\" data-sizex=\"3\" data-sizey=\"2\"><div class=\"tileImage\"><img src=\"images/story-3.png\" alt=\"story\"></div></li>\n				        	</div>\n\n				        	<div class=\"grid_16 secondRow\">\n				        		<li id=\"4\" class=\"storyTile\" data-row=\"1\" data-col=\"2\" data-sizex=\"3\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-4.png\" alt=\"story\"></div></li>\n				        		<li id=\"5\" class=\"storyTile\" data-row=\"2\" data-col=\"2\" data-sizex=\"5\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-5.png\" alt=\"story\"></div></li>\n				        	</div>\n				        \n				        	<div class=\"grid_16 thirdRow\">\n				        		<li id=\"6\" class=\"storyTile\" data-row=\"1\" data-col=\"4\" data-sizex=\"2\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-6.png\" alt=\"story\"></div></li>\n				        		<li id=\"7\" class=\"storyTile\" data-row=\"2\" data-col=\"4\" data-sizex=\"6\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-7.png\" alt=\"story\"></div></li>\n				        	</div>\n				    			    	\n					        <div class=\"grid_16 fourthRow\">\n					        	<li id=\"8\" class=\"storyTile\" data-row=\"3\" data-col=\"4\" data-sizex=\"2\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-8.png\" alt=\"story\"></div></li>\n						        <li id=\"9\" class=\"storyTile\" data-row=\"1\" data-col=\"5\" data-sizex=\"2\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-9.png\" alt=\"story\"></div></li>\n						        <li id=\"10\" class=\"storyTile\" data-row=\"3\" data-col=\"5\" data-sizex=\"2\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-9.png\" alt=\"story\"></div></li>\n			        			<li id=\"11\" class=\"storyTile\" data-row=\"1\" data-col=\"6\" data-sizex=\"2\" data-sizey=\"3\"><div class=\"tileImage\"><img src=\"images/story-9.png\" alt=\"story\"></div></li>\n					        </div>\n					    </ul>\n					</div>\n					<div class=\"one columns dustbin\">\n						<a ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "deleteNewspaper", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"right-btn\">\n						<img src=\"images/delete.png\" alt=\"delete-button\">\n						</a>\n					</div>\n				</div>\n			</div>\n			<div class=\"twelve column temporarilyHidden\">\n				<a href=\"#\" class=\"AddMore-btn right\" ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "addMoreTiles", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">+Add More</a>\n			</div>\n			\n		</div>\n		\n	</div>\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["templates/previewStory"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data
/**/) {
this.compilerInfo = [3,'>= 1.0.0-rc.4'];
helpers = helpers || Ember.Handlebars.helpers; data = data || {};
  var buffer = '', hashContexts, hashTypes, escapeExpression=this.escapeExpression;


  data.buffer.push("<div id=\"previewStory\" class=\"preivewStory\">\n	<div ");
  hashContexts = {'target': depth0};
  hashTypes = {'target': "STRING"};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "closePreview", {hash:{
    'target': ("controller")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"closePreview\">\n	</div>\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["templates/story"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data
/**/) {
this.compilerInfo = [3,'>= 1.0.0-rc.4'];
helpers = helpers || Ember.Handlebars.helpers; data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts;
  data.buffer.push("\n		");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.productThumbImageUrl", {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n		");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.productImageUrl", {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n		<h3 id=\"storyHeading\" class=\"storyHeading\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.headline", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</h3>\n		<div id=\"storyBody\" class=\"storyBody itemForPaper\">\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz", {hash:{},inverse:self.program(23, program23, data),fn:self.program(10, program10, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n		</div>\n		");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.originatorName", {hash:{},inverse:self.noop,fn:self.program(36, program36, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n		<a href=\"#\" class=\"effect-on-hover\"></a>\n		");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz", {hash:{},inverse:self.program(40, program40, data),fn:self.program(38, program38, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n	");
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<img class=\"storyIcon\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.productThumbImageUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n		");
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<img class=\"storyIcon\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.thumbImageUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n		");
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<div class=\"storyIconBigContainer\">\n				<img class=\"storyIconBig\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.productImageUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n			</div>\n		");
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<div class=\"storyIconBigContainer\">\n				<img class=\"storyIconBig\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.imageUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n			</div>\n		");
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts;
  data.buffer.push("\n\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.who", {hash:{},inverse:self.noop,fn:self.program(11, program11, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.what", {hash:{},inverse:self.noop,fn:self.program(13, program13, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.where", {hash:{},inverse:self.noop,fn:self.program(15, program15, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.when", {hash:{},inverse:self.noop,fn:self.program(17, program17, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.how", {hash:{},inverse:self.noop,fn:self.program(19, program19, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.wsBuzz.why", {hash:{},inverse:self.noop,fn:self.program(21, program21, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n				<p class=\"oped\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.oped", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n\n			");
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Who</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.who", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" </p>\n				");
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>What</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.what", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Where</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.where", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program17(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>When</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.when", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>How</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.how", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program21(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Why</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.wsBuzz.why", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program23(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts;
  data.buffer.push("\n\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.who", {hash:{},inverse:self.noop,fn:self.program(24, program24, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.what", {hash:{},inverse:self.noop,fn:self.program(26, program26, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.where", {hash:{},inverse:self.noop,fn:self.program(28, program28, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.when", {hash:{},inverse:self.noop,fn:self.program(30, program30, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.how", {hash:{},inverse:self.noop,fn:self.program(32, program32, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n				");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.why", {hash:{},inverse:self.noop,fn:self.program(34, program34, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n					<p class=\"oped\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.oped", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Who</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.who", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" </p>\n				");
  return buffer;
  }

function program26(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>What</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.what", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program28(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Where</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.where", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program30(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>When</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.when", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program32(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>How</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.how", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program34(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n					<p><b>Why</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.why", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n				");
  return buffer;
  }

function program36(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n			<div id=\"storyByline\" class=\"storyByline itemForPaper\">by ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.originatorName", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</div>\n		");
  return buffer;
  }

function program38(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<input class=\"imageId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.wsBuzz.imageId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"storyOriginalImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.wsBuzz.imageUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"storyThumbImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.wsBuzz.thumbImageUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"buzzId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.wsBuzz.buzzId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n		");
  return buffer;
  }

function program40(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n			<input class=\"imageId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.imageId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"storyOriginalImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.imageUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"storyThumbImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.thumbImageUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n			<input class=\"buzzId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.buzzId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n		");
  return buffer;
  }

function program42(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes;
  data.buffer.push("\n		<img class=\"storyIcon\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.imageThumbUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n		<div class=\"storyIconBigContainer\">\n			<img class=\"storyIconBig\" ");
  hashContexts = {'src': depth0,'alt': depth0};
  hashTypes = {'src': "STRING",'alt': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'src': ("value.imageOriginalUrl"),
    'alt': ("value.headline")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n		</div>\n		<h3 id=\"storyHeading\" class=\"storyHeading\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.headline", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</h3>\n		<div id=\"storyBody\" class=\"storyBody itemForPaper\">\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.pwho", {hash:{},inverse:self.noop,fn:self.program(43, program43, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.pwhat", {hash:{},inverse:self.noop,fn:self.program(45, program45, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.pwhere", {hash:{},inverse:self.noop,fn:self.program(47, program47, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.pwhen", {hash:{},inverse:self.noop,fn:self.program(49, program49, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.phow", {hash:{},inverse:self.noop,fn:self.program(51, program51, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n			");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.pwhy", {hash:{},inverse:self.noop,fn:self.program(53, program53, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n			<p class=\"oped\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.oped", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n		</div>\n		");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "value.author", {hash:{},inverse:self.noop,fn:self.program(55, program55, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n		<a href=\"#\" class=\"effect-on-hover\"></a>\n		<input class=\"imageId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.imageId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n		<input class=\"storyOriginalImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.imageOriginalUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n		<input class=\"storyThumbImage\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.imageThumbUrl")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n		<input class=\"buzzId\" type=\"hidden\" ");
  hashContexts = {'value': depth0};
  hashTypes = {'value': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'value': ("value.wishId")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("/>\n	");
  return buffer;
  }
function program43(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>Who</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.pwho", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" </p>\n			");
  return buffer;
  }

function program45(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>What</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.pwhat", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }

function program47(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>Where</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.pwhere", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }

function program49(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>When</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.pwhen", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }

function program51(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>How</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.phow", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }

function program53(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n				<p><b>Why</b>:&nbsp ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.pwhy", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</p>\n			");
  return buffer;
  }

function program55(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n			<div id=\"storyByline\" class=\"storyByline itemForPaper\">by ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "value.author", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</div>\n		");
  return buffer;
  }

  data.buffer.push("<li class=\"story grid_3\">\n	");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "controller.isAllStoriesMode", {hash:{},inverse:self.program(42, program42, data),fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n</li>");
  return buffer;
  
});

})();

(function() {

TNE = Ember.Application.create({});

TNE.NewspaperController = Ember.Controller.create({
  currentCatSelected:null,
  allMessageTypeActiveCategories:[],
  allOrMineStories:[],
  allRequestSummaries:[],
  allNewspapers:[],
  selectedNewspaperId:null,
  openPromptObject:null,
  currNewspaperId:0,
  isPreviewStory:false,
  isAllStoriesMode:true,
  currentTheme:"",
  defaultHeadline:"Your Brilliant Newspaper Title",
  defaultLocation:"Philadalphia PA",
  defaultEdition:"VOL 1 ISSUE-1",
  openPrompt:{headline:"Open"},
  themesObject:{

    "Helvetica":{

      "npHeadline":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"21px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"normal",
        "fontSize":"16px"
      },
      "storyByline":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Hipster":{

      "npHeadline":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"#244373",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Museo Sans",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Hard News":{

      "npHeadline":{
        "color":"#244373",
        "fontFamily":"Bebas Neue",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#244373",
        "fontFamily":"Bebas Neue",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#244373",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Sporty":{

      "npHeadline":{
        "color":"#A50F0F",
        "fontFamily":"Anton",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Scada",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#A50F0F",
        "fontFamily":"Anton",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Scada",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#A50F0F",
        "fontFamily":"Scada",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    }
  },

  newNewspaperSetup:function(){
    var newspaperId = this.get("selectedNewspaperId"),
        self = this;

    this.get("currNewspaperId",0);
    $("#newspaper").find(".story").remove();
    $('#npHeadline').html(this.get("defaultHeadline"));
    $('#npLocation').html(this.get("defaultLocation"));
    $('#npEdition').html(this.get("defaultEdition"));

    $('#npHeadline').css("color", "#222");
    $('#npHeadline').css("font-family", "'didot','Palatino Linotype','Bodoni MT',serif cursive;");
    $('#npHeadline').css("font-size", "37px");
    $('#npHeadline').css("font-weight", "normal");

    $('#npLocation').css("color", "rgb(0, 0, 0)");
    $('#npLocation').css("font-family", "'optima', 'Candara', Helvetica Light, sans-serif");
    $('#npLocation').css("font-size", "14px");
    $('#npLocation').css("font-weight", "normal");

    $('#npEdition').css("color", "rgb(0, 0, 0)");
    $('#npEdition').css("font-family", "'optima', 'Candara', Helvetica Light, sans-serif");
    $('#npEdition').css("font-size", "14px");
    $('#npEdition').css("font-weight", "normal");
    $('li.RoundedLeft select.allNewspapers option[value="new"]').attr('selected', '');
    $('li.RoundedLeft select.allNewspapers option[value="preselectHack"]').attr('selected', 'selected');

    // Commented out for indefinite time to speed things up
    /*self.get("allMessageTypeActiveCategories").forEach(function(obj){
      if(obj.id === "Breaking News"){
        self.set("currentCatSelected", obj);
      }
    });
    self.allStories();*/
    TN.utils.passiveNotification('Create new newspaper', 'You can drag and drop story items now.');
  },

  newspaperIdObserver:function(){
    var newspaperId = this.get("selectedNewspaperId"),
        self = this;
    self.set("openPrompt",this.get("openPromptObject"));

    //Handling the new newspaper case
    if(newspaperId === "new"){
      self.newNewspaperSetup();
    }

    // Handling the newspaperId case
    if( !Ember.empty(newspaperId) && newspaperId > 0 ){
      TN.services.getNewspaper(newspaperId).done(function(data){
          self.set("currNewspaperId",newspaperId);
          self.populateNewspaper(data[0]);
      }).fail(function(jqXHR, textStatus, errorThrown){
          alert('There was an error in retrieving the newspaper: (' +  jqXHR.status + ') ' + errorThrown);        
        });
    }

  }.observes("selectedNewspaperId"),

  populateNewspaper:function(newspaper){
    if(!Ember.empty(newspaper)){
      var self = this;
      $('#npHeadline').html(newspaper.headline);
      $('#npLocation').html(newspaper.location);
      $('#npEdition').html(newspaper.edition);
      $('#npHeadline').css("color", newspaper.titleFontColor);
      $('#npHeadline').css("font-family", newspaper.titleFontStyle);
      $('#npHeadline').css("font-size", newspaper.titleFontSize);

      $('#npLocation').css("color", newspaper.locEdFontColor);
      $('#npLocation').css("font-family", newspaper.locEdFontStyle);
      $('#npLocation').css("font-size", newspaper.locEdFontSize);

      $('#npEdition').css("color", newspaper.locEdFontColor);
      $('#npEdition').css("font-family", newspaper.locEdFontStyle);
      $('#npEdition').css("font-size", newspaper.locEdFontSize);

      //First clear newspaper

      $("#newspaper").find(".story").remove();

      // And then Populate Stories!
      newspaper.stories.forEach(function(story){
        //For each story fetch all the attributes and make story template i.e. html
        var who = "",
            what = "",
            when = "",
            where = "",
            how = "",
            why = "",
            authorName = "";

        if(!Ember.empty(story.who)){
         who = '<p><b>Who</b>:&nbsp' + story.who + '</p>';
        }

        if(!Ember.empty(story.what)){
         what = '<p><b>What</b>:&nbsp' + story.what + '</p>';
        }

        if(!Ember.empty(story.when)){
         when = '<p><b>When</b>:&nbsp'+ story.when +'</p>';
        }

        if(!Ember.empty(story.where)){
         where = '<p><b>Where</b>:&nbsp' + story.where + '</p>';
        }

        if(!Ember.empty(story.how)){
         how = '<p><b>How</b>:&nbsp'+ story.how +'</p>';
        }

        if(!Ember.empty(story.why)){
         why = '<p><b>Why</b>:&nbsp'+ story.why +'</p>';
        }

        if(!Ember.empty(story.authorName)){
         authorName = '<div id="storyByline" class="storyByline itemForPaper">by '+ story.authorName +'</div>';
        }

        var storyTemplate = '<li class="story grid_3">'+
        '<img class="storyIcon" src="'+ story.buzzThumbImageUrl+'" alt="' + story.headline + '">'+

        '<div class="storyIconBigContainer"><img class="storyIconBig" src="'+ story.buzzImageUrl+'" alt="' + story.headline + '"></div>' + 

        '<h3 id="storyHeading" class="storyHeading">'+story.headline+'</h3>' +
        '<div id="storyBody" class="storyBody itemForPaper"> '
          + who + what + where + when + how + why +
          '<p>'+ story.oped +'</p>' + ' </div>' +
          authorName +
        '<a href="#" class="effect-on-hover"></a>'+
        '<input class="imageId" type="hidden" value="'+ 0 +'"/>'+
        '<input class="storyOriginalImage" type="hidden" value="'+ story.buzzImageUrl +'"/>'+
        '<input class="storyThumbImage" type="hidden" value="'+ story.buzzThumbImageUrl +'"/>'+
        '<input class="buzzId" type="hidden" value="'+ story.customerBuzzId +'"/>' + '</li>';

        // Fetch coordinates of story and then fetch corresponding container and insert story in its container
        var hashForTiles = [[0,0],[0,1,4,0,6,9,11],[0,2,5,0,7],[0,3,0,0,8,10]],
            tileId = hashForTiles[story.jrow][story.jcolumn],
            tile = $("#" + tileId),
            tileObject = {target:tile},
            storyObject = $(storyTemplate);

        // Make storyObject draggable
        storyObject.draggable({
          addClasses: false,
          appendTo: "body",
          helper: "clone",
          revert: "invalid", // when not dropped, the item will revert back to its initial position
          containment: "document",
          cursor: "move"
        });

        // Put story on tile now
        TNE.NewspaperView.putStoryOnTile(tileObject,storyObject, false);
        // apply styling and all to stories
        
        tile.find('.story .storyHeading').css("color", story.fontcolor);
        tile.find('.story .storyHeading').css("font-family", story.fontstyle);
        tile.find('.story .storyHeading').css("font-size", story.fontsize);

        tile.find('.story .storyBody p').css("color", story.bodyfontcolor);
        tile.find('.story .storyBody p').css("font-family", story.bodyfontstyle);
        tile.find('.story .storyBody p').css("font-size", story.bodyfontsize);
      });
    }
  },

  applyTheme:function(event, optionalTheme){
    var idAsIndex;
    if(Ember.empty(event)){
      idAsIndex = optionalTheme;
    }
    else{
      idAsIndex = $(event.target).attr("id");
    }

    this.set("currentTheme", idAsIndex);

    var npHead = this.get("themesObject")[idAsIndex].npHeadline;
    var npLocAndEdition = this.get("themesObject")[idAsIndex].npLocAndEdition;
    var storyHeading = this.get("themesObject")[idAsIndex].storyHeading;
    var storyBody = this.get("themesObject")[idAsIndex].storyBody;
    var storyByline = this.get("themesObject")[idAsIndex].storyByline;

    $('#npHeadline').css("color", npHead.color);
    $('#npHeadline').css("font-family", npHead.fontFamily);
    $('#npHeadline').css("font-weight", npHead.fontWeight);
    $('#npHeadline').css("font-size", npHead.fontSize);

    $('#npLocation').css("color", npLocAndEdition.color);
    $('#npLocation').css("font-family", npLocAndEdition.fontFamily);
    $('#npLocation').css("font-weight", npLocAndEdition.fontWeight);
    $('#npLocation').css("font-size", npLocAndEdition.fontSize);

    $('#npEdition').css("color", npLocAndEdition.color);
    $('#npEdition').css("font-family", npLocAndEdition.fontFamily);
    $('#npEdition').css("font-weight", npLocAndEdition.fontWeight);
    $('#npEdition').css("font-size", npLocAndEdition.fontSize);

    $('.storyTile .storyHeading').css("color", storyHeading.color);
    $('.storyTile .storyHeading').css("font-family", storyHeading.fontFamily);
    $('.storyTile .storyHeading').css("font-weight", storyHeading.fontWeight);
    $('.storyTile .storyHeading').css("font-size", storyHeading.fontSize);

    $('.storyTile .storyBody p').css("color", storyBody.color);
    $('.storyTile .storyBody p').css("font-family", storyBody.fontFamily);
    $('.storyTile .storyBody p').css("font-weight", storyBody.fontWeight);
    $('.storyTile .storyBody p').css("font-size", storyBody.fontSize);

    $('.storyTile .storyByline').css("color", storyByline.color);
    $('.storyTile .storyByline').css("font-family", storyByline.fontFamily);
    $('.storyTile .storyByline').css("font-weight", storyByline.fontWeight);
    $('.storyTile .storyByline').css("font-size", storyByline.fontSize);

  },

  applyTemplate:function(){
    //code for applying template
  },

  categoryObserver:function(){
    var category = this.get("currentCatSelected");

    if(!Ember.empty(category)){
      if(this.get("isAllStoriesMode") === true){
        this.allStories();
      }
      else{
        this.mineStories();
      }
    }

  }.observes("currentCatSelected"),

  initialise:function(){
    var self = this;
    var catDropDown = $('#Categories');
    var catListElem = $('#storyCatsDropDown');
    var usersNewspapersSelect = $('li.RoundedLeft select.allNewspapers');
    
    function loadCategories(){
    	
        var csCatDropdown = $('#storyCatsDropDown > ul');
        function addCategoryItem(itemText){
        	var categoryItemElem = null;
        	
        	if( itemText === 'All Categories' ){
            	categoryItemElem = $('<li id="allCatsOption">' + itemText + '</li>');           		
        	} else {
            	categoryItemElem = $('<li>' + itemText + '</li>');
        	}            	
        	
        	categoryItemElem.click(function(){
        		var currElem = $(this);
        		var currLabel = currElem.text();
        		var currId = TN.categories.getValue(currElem.text());
                TN.utils.setDropdownValue(catListElem, currLabel);            		
                
                self.set("currentCatSelected", {id:currId, label:currLabel});              
                
                if(this.get("isAllStoriesMode") === true){
                    this.allStories();
                  }
                  else{
                    this.mineStories();
                  }
                
        		$('body').click();
        		return false;        	
        	});
        	
        	csCatDropdown.append(categoryItemElem);
        }
                
        // addCategoryItem('All Categories');
        // TN.utils.setDropdownValue(catListElem, 'All Categories');
    	
        TN.services.getAllMessageTypeActiveCategories().done(function(json){
            if( !!json ){
                var maxItems = json.length;
                
                for( var i = 0; i < maxItems; i++ ){
                    if (json[i].id == "Q") continue;
                    if( json[i].global === 1 || json[i].global === 0 ){
						var currCat = json[i].id;
						var currCatLabel = TN.categories.getLabel(currCat);
						if( !!currCatLabel ){
	                    	addCategoryItem(currCatLabel);							
						}
                    }
                }                 
                
                TN.utils.setDropdownValue(catListElem, 'Trending News');            		
            }
        });
    }

    this.getNewspapers();
    $.ajax({
      type: 'GET',
      data: 'action=getAllMessageTypeActiveCategories',
      url : '/salebynow/json.htm',
      success:function(categories){
    	  var mappedCategories = [];
    	  var numCats = categories.length;
    	  
    	  for( var i = 0; i < numCats; i++ ){
    		  var currCatValue = categories[i].id;
    		  var currLabel = TN.categories.getLabel(currCatValue);
    		  if( !!currLabel ){
    			  catDropDown.append('<option value="' + currCatValue + '">' + currLabel + '</option>').trigger('change');
    			  mappedCategories.push({id:currCatValue,label:currLabel});
    		  }
    	  }
    	  
		  catDropDown.val('Breaking News').trigger('change');
    	  self.set("allMessageTypeActiveCategories", Ember.ArrayController.create({content:mappedCategories}));
    	  self.get("allMessageTypeActiveCategories").forEach(function(obj){
          if(obj.id === "Breaking News"){
            self.set("currentCatSelected", obj);
          }
        });
      }
    });
    
    loadCategories();
    
    $('#storiesSearch').keyup(function(event){
      if( event.which === 13 ){
        self.searchForStories();
      }
    });

    usersNewspapersSelect.click(function(event){
      if (event.currentTarget.value == "new") {
        self.newNewspaperSetup();
      }
    });

  },

  allStories:function(event){
    this.set("allOrMineStories",[]);
    this.set("isAllStoriesMode", true);
    var custId = TN.utils.getCookie('TNUser'),
    	totalPages = 0,
        pageNum = 1,
        category,
        numItems = 25,
        self = this,
        storiesArray = [];

    function getItems(){
        TN.services.getAllRequestSummaries(custId, numItems, pageNum, category).
      	done(function(data){
      		totalPages = data[0].noOfPages;
            $("#allStories").css("background", "rgb(236,96,63)");
            $("#mine").css("background", "");
            $('#storiesSearch').val("");
            storiesArray = storiesArray.concat(data[0].fittingRoomSummaryList);
            
            pageNum++;
            if( pageNum <= totalPages ){
            	getItems();
            }
            else{
                self.set("allOrMineStories", storiesArray);            	
            }
      	});    	
    }
    
    if(!Ember.empty(this.get("currentCatSelected"))){
      category = this.get("currentCatSelected").id;
      
      getItems();
    }
    else{
      alert("Please select a category first!");
    }
  },

  mineStories:function(){
      this.set("allOrMineStories",[]);
      this.set("isAllStoriesMode", false);
      var custId = TN.utils.getCookie('TNUser'),
      	  totalPages = 0,
          pageNum = 1,
          numItems = 25,
          category,
          self = this,
          storiesArray = [];
      
      function getItems(){
          TN.services.getStoriesAndWishListFiltered(custId, numItems, pageNum, 'mine', category).
          	done(function(data){
          	  totalPages = data[0].numPages;
          	  $("#mine").css("background", "rgb(236,96,63)");
              $("#allStories").css("background", "");
              $('#storiesSearch').val("");
              
              storiesArray = storiesArray.concat(data[0].stories);
              
              pageNum++;
              
              if( pageNum <= totalPages ){
            	  getItems();
              }
              else {
                  self.set("allOrMineStories",storiesArray);            	  
              }
            });    	  
      }
      
      if(!Ember.empty(this.get("currentCatSelected"))){
        category = this.get("currentCatSelected").id;
        
        getItems();
      }
      else{
        alert("Please select a category first!");
      }
  },
  
  searchForStories:function(event){
    this.set("allOrMineStories",[]);
    var searchText = $('#storiesSearch').val();
    var self = this;
    
    if( !!searchText ){
      TN.services.searchStories( searchText, 1, 20 ).
      done(function(data){
    	  self.set("isAllStoriesMode", true);
          $("#allStories").css("background", "");
          $("#mine").css("background", "");
          self.set("allOrMineStories", data);       
      });     
    }
  },
  
  deleteNewspaper:function(event){
  	if( TN.header.isGuestUser() ) return;
	  	
    var self = this;
    var currNewspaperId = self.get("currNewspaperId");
    
    if( currNewspaperId > 0 ){
      TN.services.deleteNewspaper(Number(currNewspaperId)).done(function(){
        self.getNewspapers();
          $("#newspaper").find(".story").remove();
        self.set('currNewspaperId',0);
        self.set('selectedNewspaperId',0);
        alert('Newspaper has been successfully deleted.');
      }).fail(function(jqXHR, textStatus, errorThrown){
        if( jqXHR.responseText === 'success'){
          self.getNewspapers();
          $("#newspaper").find(".story").remove();
          self.set('currNewspaperId',0);
          self.set('selectedNewspaperId',0);
          $('#npHeadline').html(self.get("defaultHeadline"));
          $('#npLocation').html(self.get("defaultLocation"));
          $('#npEdition').html(self.get("defaultEdition"));
          alert('Newspaper has been successfully deleted.');
        } else {
          alert('Error deleting newspaper:' + errorThrown );
        }
      });
    }
    else {
      alert('No newspaper is currently loaded.');
    }
  },

  themesAction:function(event){
    $("#stories").hide();
    $("#themes").show();
    $("#templates").hide();
  },

  storiesAction:function(event){
    $("#stories").show();
    $("#themes").hide();
    $("#templates").hide();
  },

  templatesAction:function(event){
    $("#templates").show();
    $("#stories").hide();
    $("#themes").hide();
  },

  getNewspapers:function(){
    var self = this,
        custId = TN.utils.getCookie('TNUser'),
        newNewspaperObject = {id:"new", headline:"Create New"},
        newPreselectedObject = {id:"preselectHack"};

    self.set("allNewspapers", []);
    TN.services.getNewspapers(custId).done(function(data){
      self.set("allNewspapers", data);
      self.get("allNewspapers").reverse();
      self.get("allNewspapers").addObject(newNewspaperObject);
      self.get("allNewspapers").addObject(newPreselectedObject);
      self.get("allNewspapers").reverse();
    }).fail(function(){
      self.get("allNewspapers").addObject(newPreselectedObject);
      self.get("allNewspapers").addObject(newNewspaperObject);
    });
  },


  expandContainer:function(){
    if($(".storiesContainer").css("height") === "220px"){
      $(".storiesContainer").animate({"height": "500px"}, {duration: "slow" });
      $(".storiesContainer").addClass("storiesContainerExpanded");
    }
    else{
      $(".storiesContainer").animate({"height": "220px"}, {duration: "slow" });
      $(".storiesContainer").scrollTop(0);
      $(".storiesContainer").removeClass("storiesContainerExpanded");     
    }
  },

  getSelectedCat:function(){
    return "";
  },

  navBarAction:function(event){
    /*$(event.target).parent().find(".navBarActive").removeClass("navBarActive");
    $(event.target).addClass("navBarActive");*/
  },

  addMoreTiles:function(){

    //Take the id of the last tile
    var lastTileId = parseInt($(".tileImage").last().parent().attr("id"));

    var firstRow = '<div class="grid_16 firstRow">'+
                      '<li id="'+ (lastTileId+1) + '" class="storyTile" data-row="1" data-col="1" data-sizex="5" data-sizey="4"><div class="tileImage"><img src="images/story-1.png" alt="story"></div></li>'+
                      '<li id="'+ (lastTileId+2) + '" class="storyTile" data-row="2" data-col="1" data-sizex="3" data-sizey="2"><div class="tileImage"><img src="images/story-2.png" alt="story"></div></li>'+
                      '<li id="'+ (lastTileId+3) + '" class="storyTile" data-row="3" data-col="1" data-sizex="3" data-sizey="2"><div class="tileImage"><img src="images/story-3.png" alt="story"></div></li>'+
                    '</div>'

    var secondRow ='<div class="grid_16 secondRow">'+
                        '<li id="'+ (lastTileId+4) + '" class="storyTile" data-row="1" data-col="2" data-sizex="3" data-sizey="3"><div class="tileImage"><img src="images/story-4.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+5) + '" class="storyTile" data-row="2" data-col="2" data-sizex="5" data-sizey="3"><div class="tileImage"><img src="images/story-5.png" alt="story"></div></li>'+
                      '</div>'
                
    var thirdRow = '<div class="grid_16 thirdRow">'+
                        '<li id="'+ (lastTileId+6) + '" class="storyTile" data-row="1" data-col="4" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-6.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+7) + '" class="storyTile" data-row="2" data-col="4" data-sizex="6" data-sizey="3"><div class="tileImage"><img src="images/story-7.png" alt="story"></div></li>'+
                      '</div>'
                        
    var fourthRow = '<div class="grid_16 fourthRow">'+
                        '<li id="'+ (lastTileId+8) + '" class="storyTile" data-row="3" data-col="4" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-8.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+9) + '" class="storyTile" data-row="1" data-col="5" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+10) + '" class="storyTile" data-row="3" data-col="5" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+11) + '" class="storyTile" data-row="1" data-col="6" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                      '</div>'

    $("#gridsterList").append(firstRow);
    $("#gridsterList").append(secondRow);
    $("#gridsterList").append(thirdRow);
    $("#gridsterList").append(fourthRow)
    
    var self = this,
        newspaperTile = $(".gridster");
    $( "li", newspaperTile ).droppable({
        accept: "li",
        activeClass: "",
        drop: function( event, ui ) {
          TNE.NewspaperView.resizeStory( event, ui.draggable );
        }
      });
  },

  previewHelper:function(){
    $("#previewStory").append($('#newspaper').clone());
    $("#previewStory").find(".preview").remove();
    $("#previewStory").find(".dustbin").remove();
    $("#previewStory .storyTile").remove();

    $("#previewStory ul .firstRow").remove();
    $("#previewStory ul .secondRow").remove();
    $("#previewStory ul .thirdRow").remove();
    $("#previewStory ul .fourthRow").remove();

    $("#previewStory ul").append($("#newspaper .storyTile .story").parent().clone());
    $("#previewStory .storyTile .tileImage").remove();
    //$("#previewStory")
    $("#previewStory .storyTile").find(".itemForPaper").show();
    $("#previewStory .storyTile .story").addClass("storyOnDrop2");
    $("#previewStory .storyTile").addClass("previewStoryTile");
  },


  buildStories:function(storiesArray){

    this.previewHelper();

    $('#previewStory #gridsterList').children().each(function(index){
      var currElem = $(this);
      var story = {
          'buzzImageId': currElem.find('.imageId').val(),
          'buzzImageUrl' : escape(currElem.find('.storyOriginalImage').val()),
          'buzzThumbImageUrl' : escape(currElem.find('.storyThumbImage').val()),
          'customerBuzzId' : currElem.find('.buzzId').val(),
          'elementId' : 0,
          'fontcolor' : currElem.find('.storyHeading').css('color'),
          'fontsize' : currElem.find('.storyHeading').css('font-size'),
          'fontstyle' : currElem.find('.storyHeading').css('font-family'),
          'bodyfontcolor' : currElem.find('.storyBody p').css('color'),
          'bodyfontsize' : currElem.find('.storyBody p').css('font-size'),
          'bodyfontstyle' : currElem.find('.storyBody p').css('font-family'),
          'bylinefontcolor' : currElem.find('.storyByline').css('color'),
          'bylinefontsize' : currElem.find('.storyByline').css('font-size'),
          'bylinefontstyle' : currElem.find('.storyByline').css('font-family'),
          'index' : index,
          'xpos' : parseFloat(currElem.position().left),
          'ypos' : parseFloat(currElem.position().top),
          'pixelWidth' : parseFloat(currElem.css('width')),
          'pixelHeight' : parseFloat(currElem.css('height')),
          'jcolumn' : parseFloat(currElem.attr('data-col')),
          'jheight' : parseFloat(currElem.attr('data-sizey')),
          'jrow' : parseFloat(currElem.attr('data-row')),
          'jwidth' : parseFloat(currElem.attr('data-sizex'))
      };        
      storiesArray.pushObject(story);
    });
    
    $("#previewStory #newspaper").remove();
  },

  previewStory:function(){
    if($(".storyTile").find(".story").length === 0){
      alert("Please add a story to newspaper!");
    }
    else{
      this.previewHelper();
      this.set("isPreviewStory",true);
      $(".previewStoryView").css("visibility", "visible");
      $("#ember-newspaper").addClass("fadeBackground");
    }
  },

  closePreview:function(){
    this.set("isPreviewStory",false);
    $(".previewStoryView").css("visibility", "hidden");
    $("#previewStory #newspaper").remove();
    $("#ember-newspaper").removeClass("fadeBackground");
  },

  editText:function(event){
    if(this.get("isPreviewStory") === false){
      $(event.target).attr('contentEditable','true');
    }
  },

  saveNewspaper:function(event){
  	if( TN.header.isGuestUser() ) return;
	  	
    if($(".storyTile").find(".story").length === 0){
      alert("Please add a story to newspaper!");
    }
    else{
      var today = new Date();
      var storiesArray = [];    
      this.buildStories(storiesArray);    
      
      var npHeadlineElem = $('#npHeadline');
      var npLocationElem = $('#npLocation');
      var npEditionElem = $('#npEdition');
      
      var newspaper = {
            'custId' : TN.utils.getCookie('TNUser'),
            'custName' : $('#firstname').text(),
            'date' : today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear(),
            'edition' : npEditionElem.text(),
            'headline' : npHeadlineElem.text(),
            'titleFontColor' : npHeadlineElem.css('color'),
            'titleFontStyle' : npHeadlineElem.css('font-family'),
            'titleFontSize' : npHeadlineElem.css('font-size'),
            'locEdFontColor' : npLocationElem.css('color'),
            'locEdFontStyle' : npLocationElem.css('font-family'),
            'locEdFontSize' : npLocationElem.css('font-size'),
            'id' : this.get("currNewspaperId"),
            'location' : npLocationElem.text(),
            'stories' : storiesArray
      };
      
      if( this.get("currNewspaperId") > 0 ){
        var self = this;
        TN.services.updateNewspaper(newspaper).done(function(response){
          if( response[0] === true ){
            /*this.loadNewspapersList();
            this.loadNewspaper(this.get("currNewspaperId"));*/
            alert('Newspaper has been successfully updated.');
          }
          else {
            alert('There was an error in making the updates');
          }
        }).fail(function(jqXHR, textStatus, errorThrown){
          alert('There was an error in making the updates: (' +  jqXHR.status + ') ' + errorThrown);        
        });
      }
      else {
        var self = this;
        TN.services.saveNewspaper(newspaper).done(function(response){
          /*this.loadNewspapersList();
          this.loadNewspaper(parseFloat(response));*/
          self.set("currNewspaperId",response[0]);
          self.getNewspapers();
          alert('Newspaper has been successfully saved.');
        }).fail(function(jqXHR, textStatus, errorThrown){
          alert('There was an error in saving the newspaper: (' +  jqXHR.status + ') ' + errorThrown);
        }); 
      }
    }
  },

  publishNewspaper:function(){
  	if( TN.header.isGuestUser() ) return;
	  	
    if (this.get("currNewspaperId") != 0) {
      var newspaperHeadline = $('#npHeadline').text();
      TN.sharingHandler.initNPShare(this.get("currNewspaperId"), newspaperHeadline);
    }
    else{
      if($(".storyTile").find(".story").length === 0){
        alert("Please load a newspaper to share first.");
      }
      else{
        alert('Please save your newspaper first!');
      }
    }
  }

});

TNE.NewspaperView = Ember.View.create({
    templateName:"templates/newspaper",
    controllerBinding:"TNE.NewspaperController",

    didInsertElement:function(){

      this.get("controller").initialise();

      var self = this,
          newspaperTile = $(".gridster");

      $( "li", newspaperTile ).droppable({
        accept: "li",
        activeClass: "",
        drop: function( event, ui ) {
          self.resizeStory( event, ui.draggable );
        }
      });

    },

    putStoryOnTile:function(event, draggedObj, shouldApplyTheme){
      // console.log( $(event.target).css("height"), $(event.target).css("width"), $(event.target).height(), $(event.target).width() );
      
      // Normalize image dimensions according to div "window" .storyIconBigContainer which is set to overflow:hidden
      // This is equivalent to TN.utils.normalizeImage function which is used to "normalize" image dimensions in simpler circumstances.
      // Commented console.log lines are deliberately left out for inspection needs.
      var storyIconBigImg = $(draggedObj).find('img.storyIconBig');
      // http://css-tricks.com/snippets/jquery/get-an-images-native-width/
      // Create new offscreen image to test
      var theImage = new Image();
      var targetWidth = $(event.target).css("width");
      var targetHeight = $(event.target).css("height");
      // 16 pixels total to decrement before calculating image container window (.storyIconBigContainer div's) "width factor"
      // comes from 2x7 pixels for left and rigth padding respectively and from additional 2x1 pixels for 1 pixel thick .story border
      var windowWidth = $(event.target).width() - 16;
      var windowHeight = ($(event.target).height() * 0.8) - 16;      
      var imgContainerWindowWidthFactor = windowWidth / windowHeight;

      theImage.onload = function() {
        // Get accurate measurements from that.
        var originalImgWidth = theImage.width;
        var originalImgHeight = theImage.height;
        var origImgWidthFactor = originalImgWidth/originalImgHeight;
        // console.log('targetWidth: '+targetWidth, 'targetHeight: '+targetHeight);
        // console.log('originalImgWidth: '+originalImgWidth, ' originalImgHeight: '+originalImgHeight, ' windowWidth: '+windowWidth, ' windowHeight: '+windowHeight, 'origImgWidthFactor: '+origImgWidthFactor, 'imgContainerWindowWidthFactor: '+imgContainerWindowWidthFactor);
        if(origImgWidthFactor > imgContainerWindowWidthFactor){
          storyIconBigImg.height(windowHeight);
          //console.log('setting new width: ' + (storyIconBigImg.height() * origImgWidthFactor));
          storyIconBigImg.width( storyIconBigImg.height() * origImgWidthFactor );
          // console.log('new width: ' + storyIconBigImg.width() );
          var trailingWidth = storyIconBigImg.height() * origImgWidthFactor - windowWidth;
          if (trailingWidth > 1) storyIconBigImg.css('position', 'relative').css('right', trailingWidth/2);
        }
        else if( origImgWidthFactor < imgContainerWindowWidthFactor ){
          storyIconBigImg.width(windowWidth);
          // console.log('setting new height: ' + (storyIconBigImg.width() / origImgWidthFactor));
          storyIconBigImg.height( storyIconBigImg.width() / origImgWidthFactor );
          storyIconBigImg.css('position', 'relative').css('right', 0);
        }
        else {
          storyIconBigImg.height(windowHeight);
          storyIconBigImg.width(windowWidth);
          storyIconBigImg.css('position', 'relative').css('right', 0);
        }
      };
      
      theImage.src = storyIconBigImg.attr("src");
      $(event.target).append(draggedObj);
      
      $(draggedObj).css("width", targetWidth);
      $(draggedObj).css("height", targetHeight);
      
      $(draggedObj).addClass("storyOnDrop");
      $(draggedObj).append('<div class="removeOverlay"><img src="images/close-icon-hover.png" class="removeStory"></div>');
      if($(draggedObj).find(".tileNumber").length === 0){
        $(draggedObj).append('<div class="tileNumber"></div>');
        $(draggedObj).find(".tileNumber").html($(event.target).attr("id"));
      }
      else{
        $(draggedObj).find(".tileNumber").html($(event.target).attr("id"));
      }
      $(event.target).find(".storyIcon").hide();
      $(event.target).find(".storyIconBigContainer").show();
      //$(event.target).find(".storyIconBig").css("height", "80%");
      $(event.target).find("h3").css("font-size", "20px");
      

      if(shouldApplyTheme){
        if(!Ember.empty(this.get("controller").get("currentTheme"))){
          this.get("controller").applyTheme(null, this.get("controller").get("currentTheme"));
        }
      }

      $(draggedObj).hover(function(){
        $(draggedObj).find(".removeOverlay").show();
      },
      function(){
        $(draggedObj).find(".removeOverlay").hide();
      });

      $(draggedObj).find(".removeStory").click(function(event){
        var obj = $(event.target).parent().parent();
        $("#stories").append(obj);

        obj.removeClass("storyOnDrop");
        obj.removeAttr("style");
        obj.find(".removeOverlay").remove();
        obj.find(".storyIcon").show();
        obj.find(".storyIconBigContainer").hide();
        obj.find("h3").css("color", "#333");
        obj.find("h3").css("font-size", "12px");
        obj.find("h3").css("font-weight", "normal");
        obj.find("h3").css("font-family", "'News Cycle', Helvetica Light, sans-serif");
        obj.draggable("enable");

      });
    },

    resizeStory:function(event, draggedObj){
      
      if($(event.target).find(".story").length === 0){
        this.putStoryOnTile(event, draggedObj,true);
      }
      else{
        if(!$(draggedObj).hasClass("storyOnDrop")){
          //move to the first empty tile!
          var firstEmptyTile = $('.storyTile').filter(function() { return !($(this).children().hasClass("storyOnDrop")); }).first();
          var targetObj = {target:firstEmptyTile};
          this.putStoryOnTile(targetObj,draggedObj,true);
        }
        else{
          //swap the stories!
          var tempStory = $(event.target).find(".story");
          var idOfInitialContainer = $(draggedObj).find(".tileNumber").html();
          this.putStoryOnTile(event, draggedObj,true);
          this.putStoryOnTile({target:$("#" + idOfInitialContainer)}, tempStory,true);
        }
      }
    }

});

TNE.StoryView = Ember.View.extend({
  
  templateName:"templates/story",
  
  didInsertElement : function(){

    var stories = $("#draggable");
    $( "li", stories ).draggable({
      addClasses: false,
      appendTo: "body",
          helper: "clone",
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: "document",
        cursor: "move"
    });

   /* $(".gridster ul").gridster({
        widget_margins: [5, 15],
        widget_base_dimensions: [100, 100]
      });*/
  }
});

TNE.PreviewStoryView = Ember.View.create({
  templateName:"templates/previewStory",
  controllerBinding:"TNE.NewspaperController",
  classNames:"previewStoryView",
  didInsertElement:function(){
    $(".previewStoryView").css("visibility", "hidden");
  }
});

TNE.NewspaperView.appendTo('#ember-newspaper');
TNE.PreviewStoryView.appendTo('#modalDiv');

})();