if (!TN) var TN= {};
if (!TN.sharingHandler) TN.sharingHandler= {};

(function($sharingHandler){
    
    //var custId = TN.utils.getCookie('TNUser');
    var newspaperShareInProgress = false;
    var shareInProgress = false;

    function extractShareAddresses(){
        var input = $('.TN-AF_searchBox').val();
        var output = '[';
        var emailArray = input.split(/[\s,;]+/);
        var first = true;
        jQuery.each(emailArray, function(index, val){
            if (first) output += '"'+val+'"';
            else output += ',"'+val+'"';
            first = false;
        });
        return output+']';
    };
    
    function extractShareAddressesAsArray(){
        var input = $('.TN-AF_searchBox').val();
        var emailArray = input.split(/[\s,;]+/);
        return emailArray;
    };
    
    $sharingHandler.initNPShare = function(npid, newspaperHeadline, firstImageUrl, trackAnalytics){
        
        var shareBoxCont = $('<div class="reveal-modal" style="padding: 30px">\
            <a class="close-reveal-modal"></a>\
            <h3 style="text-transform:none">Share with Friends</h3>\
            <br>\
            <ul class="TN-addFriend_Cont_list block">\
                <li><a id="fbShare" href="javascript:void(0);"><img title="Facebook" alt="facebook" src="images/addfriend/addfriends_fb_icon.png"></a></li>\
                <li><a href="javascript:alert(\'Coming soon!\')"><img title="Twitter" alt="twitter" src="images/addfriend/addfriends_tw_icon.png"></a></li>\
            </ul>\
            <h3 style="text-transform:none">or enter email address</h3>\
            <div class="TN-addFriend_Cont_searchBox block">\
                <form method="get" action="#">\
                    <input type="text" class="TN-AF_searchBox" value="" name="searchbox">\
                </form>\
            <!--input type="button" id="shareButton" class="darkBlue-button" style="float:right" value="SHARE"-->\
            <span id="share_status_ticker"></span>\
            <img src="images/addfriend/share_button.png" id="shareButton" style="float:right; cursor:pointer">\
        </div>');

//        shareBoxCont.find('.TN-addFriend_Cont_list').find('li a').click(function(event){
//            event.preventDefault();
//            alert("Coming soon.");
//        });

        shareBoxCont.find('form').submit(function(event){
            event.preventDefault();
        });

        shareBoxCont.find('.TN-addFriend_Cont_searchBox').keyup(function(event){
            if( event.which === 13 ) {
                $('#shareButton').click();
            }
        });

        shareBoxCont.find('#fbShare').click(function(){
            if (!newspaperShareInProgress) {
            	newspaperShareInProgress = true;
                $('#share_status_ticker').text('Sharing..');
                TN.services.getServerAddress().done(function(msg){
                    var fullyQualifiedImgUrl = 'http://'+msg+ firstImageUrl;
                    var postInfo = {
                    		'url' : fullyQualifiedImgUrl,
                    		'headline' : (newspaperHeadline ? newspaperHeadline : "Grab Your Towel Newspaper"),
                    		'npId' : npid,
                    		'messageId' : null
                    };
                    
                    TN.FB.post(postInfo, function(response){
                    	if( !!response ){
                        	if( !!response.error ){
                        		alert("Facebook Post error: " + response.error.message);
                        	}
                        	else {
                        		alert("This newspaper has been successfully posted on your Facebook wall.");
                                if( !!trackAnalytics ){
                                    trackAnalytics('Facebook');
                                }
                        	}                    		
                    	}
                        $('#share_status_ticker').text('');
                        newspaperShareInProgress = false;
                    });
                });
            	
            }     	
        });
        
        shareBoxCont.find('#shareButton').click(function(){
            if (!newspaperShareInProgress) {
                newspaperShareInProgress = true;
                $('#share_status_ticker').text('Sharing..');
                TN.services.shareNPTemplateInfoViaEmail(npid, newspaperHeadline, extractShareAddresses()).done(function(msg){
                    if (!!msg && !!msg[0]) {
                        if ( !( (msg[0] == "false") || (msg[0] == false) ) ) {
                            alert("Newspapers successfully shared.");
                            if( !!trackAnalytics ){
                                trackAnalytics('Standard');
                            }
                        }
                        else alert("There was a problem with sharing of newspapers.");
                    };
                }).always(function(){
                    newspaperShareInProgress = false;
                    $('#share_status_ticker').text('');
                });
            }
            else return;
        });

        $('body').append(shareBoxCont);
        shareBoxCont.reveal({"closed":function(){
        	newspaperShareInProgress = false;
        	shareBoxCont.empty().remove();
            // barvea - Temporarily commented this line out since it causes error when from the Make Newspaper page
            // TN.newspaperViewLB.init(npid);
            }
        });

    };

    $sharingHandler.initStoryShare = function(infoStruct, trackAnalytics){

        var shareBoxCont = $('<div class="reveal-modal" style="padding: 30px">\
            <a class="close-reveal-modal"></a>\
            <h3 style="text-transform:none">Share with Friends</h3>\
            <br>\
            <ul class="TN-addFriend_Cont_list block">\
                <li><a id="fbShare" href="javascript:void(0);"><img title="Facebook" alt="facebook" src="images/addfriend/addfriends_fb_icon.png"></a></li>\
                <li><a href="javascript:alert(\'Coming soon!\')"><img title="Twitter" alt="twitter" src="images/addfriend/addfriends_tw_icon.png"></a></li>\
            </ul>\
            <h3 style="text-transform:none">or enter email address</h3>\
            <div class="TN-addFriend_Cont_searchBox block">\
                <form method="get" action="#">\
                    <input type="text" class="TN-AF_searchBox" value="" name="searchbox">\
                </form>\
            <!--input type="button" id="shareButton" class="darkBlue-button" style="float:right" value="SHARE"-->\
            <span id="share_status_ticker"></span>\
            <img src="images/addfriend/share_button.png" id="shareButton" style="float:right; cursor:pointer">\
        </div>');

//        shareBoxCont.find('.TN-addFriend_Cont_list').find('li a').click(function(event){
//            event.preventDefault();
//            alert("Coming soon.");
//        });

        shareBoxCont.find('form').submit(function(event){
            event.preventDefault();
        });

        shareBoxCont.find('.TN-addFriend_Cont_searchBox').keyup(function(event){
            if( event.which === 13 ) {
                $('#shareButton').click();
            }
        });

        shareBoxCont.find('#fbShare').click(function(){
            if (!shareInProgress) {
                shareInProgress = true;
                $('#share_status_ticker').text('Sharing..');
                TN.services.getServerAddress().done(function(msg){
                    var fullyQualifiedImgUrl = 'http://'+msg+ infoStruct.productImageUrl;
                    var postInfo = {
                    		'url' : fullyQualifiedImgUrl,
                    		'headline' : (!!infoStruct.wsBuzz.headline ? infoStruct.wsBuzz.headline : "Grab Your Towel Story"),
                    		'messageId' : infoStruct.wsMessage.id,
                    		'npId' : null
                    };
                    
                    TN.FB.post(postInfo, function(response){
                    	if( !!response ){
                        	if( !!response.error ){
                        		alert("Facebook Post error: " + response.error.message);
                        	}
                        	else {
                        		alert("This story has been successfully posted on your Facebook wall.");
                                if( !!trackAnalytics ){
                                    trackAnalytics('Facebook');
                                }
                        	}                    		
                    	}
                        $('#share_status_ticker').text('');
                        shareInProgress = false;
                    });
                });
            	
            }
        	
        });
        
        shareBoxCont.find('#shareButton').click(function(){
            if (!shareInProgress) {
                shareInProgress = true;
                $('#share_status_ticker').text('Sharing..');
                                            
                TN.services.getServerAddress().done(function(msg){
                    var fullyQualifiedImgUrl = 'http://'+msg+ infoStruct.productImageUrl;
                    TN.services.sharePhotoInfoViaEmail(extractShareAddressesAsArray(), '', infoStruct.wsMessage.messageType,
                        (!!infoStruct.wsBuzz.headline) ? infoStruct.wsBuzz.headline : "Test Headline", fullyQualifiedImgUrl, '',
                        infoStruct.wsBuzz.oped, infoStruct.wsBuzz.who, infoStruct.wsBuzz.what, infoStruct.wsBuzz.when, infoStruct.wsBuzz.where,
                        infoStruct.wsBuzz.how, infoStruct.wsBuzz.why, infoStruct.wsBuzz.buzzId)
                        .done(function(msg){
                            //alert("Server response: "+msg);
                            if (msg[0] != "false") {
                                alert("Story successfully sent");
                                if( !!trackAnalytics ){
                                    trackAnalytics('Standard');
                                }
                            }
                            else alert("There was a problem with sharing of this story.");
                            })
                        .fail(function(){
                            alert("There was a problem with sharing of this story.");
                        }).always(function(){
                            shareInProgress = false;
                            $('.TN-AF_searchBox').val('');
                            $('#share_status_ticker').text('');
                        }); 
                });
            }
            else return;
        });

        $('body').append(shareBoxCont);
        // Opening a new Reveal automatically closes any other opened Reveal (most likely story Reveal) 
        shareBoxCont.reveal({"closed":function(){
        	shareInProgress = false;
        	shareBoxCont.empty().remove();
            }
        });

    };

}(TN.sharingHandler));