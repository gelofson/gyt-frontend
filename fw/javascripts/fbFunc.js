if (!TN) var TN= {};
if (!TN.FB) TN.FB= {};

(function($fb){
	var facebookLoggingIn = false;
	
	// Additional JS functions here
	window.fbAsyncInit = function() {
		FB.init({	
			appId      : '453204974697950', // App ID
			channelUrl : 'channel.html', // Channel File
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true  // parse XFBML
		});

	// Additional init code here
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				// connected
			} else if (response.status === 'not_authorized') {
				//login();
				// not_authorized
			} else {
				//login();
				// not_logged_in
			}
		});
	};

	// Load the SDK Asynchronously
	(function(d){
	  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	  if (d.getElementById(id)) {return;}
	  js = d.createElement('script'); js.id = id; js.async = true;
	  js.src = "//connect.facebook.net/en_US/all.js";
	  ref.parentNode.insertBefore(js, ref);
	}(document));
	
//	function getAuthenticate(facebook_auth_response){
//		  $.ajax({
//		    url: "/php/facebook_authenticate.php",
//		    type: "POST",
//		    data: {facebook_id : facebook_auth_response.id},
//		    dataType: "html"
//		  }).done(function(userId){
//		    $('#signInTicker').text('');
//		    
//            $.ajaxSetup({ cache: false, dataType: 'json' });
//            TN.services.loginCustomer('', userId, 'qwe').done(function(msg){
//                //console.log(typeof msg[0]);
//                if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
//                    facebookUser = true;
//                    TN.utils.setCookie("TNUser", userId, null, 345600);
//                    if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
//                    // window.location.href='./' + location.search; //
//                    var gaResp = _gaq.push(['_trackEvent', 'Join', 'FB Sign In', 'User: ' + userId]);
//                    location.href="index.html";
//                }
//                else {
//                	alert ("Error occured");
//    				// $('#fbLogin').text('Login via Facebook');
//    				facebookLoggingIn = false;
//                }
//            }).fail(function(msg){
//				// $('#fbLogin').text('Login via Facebook');
//				facebookLoggingIn = false;
//                jQuery.each( msg, function(key, value){ if (key == "status") loginfail = value; });
//                if (loginfail == 401) alert('Unable to log in.  Please make sure this Facebook account is registered with TinyNews');
//                else if (loginfail == 500) alert('Unable to log in.  Please make sure this Facebook account is registered with TinyNews');
//                else if (loginfail == 404) {
//                	alert('Unable to log in.  Please make sure this Facebook account is registered with TinyNews');
//                }
//                else alert('Unable to log in.  Please make sure this Facebook account is registered with TinyNews');
//            });
//            
//		  }).fail(function(userId){
//				// $('#fbLogin').text('Login via Facebook');
//				facebookLoggingIn = false;
//				alert('Error occured while authenticating your Facebook account.  Please try again later');
//		  });
//		}
	
	function loginFBUser(userId, userPicUrl){
        TN.services.loginCustomer('', userId, 'qwe').done(function(msg){
            if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
            	TN.utils.setCookie("TNUser", userId, null, 345600);
	          	TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
	            TN.services.sendBuzzRequest(userId, "Message Board", "Q", userPicUrl).done(function(){
	                // window.location.href='../' + location.search; // 
	                _gaq.push(['_trackEvent', 'Join', 'FB Sign In', 'User: ' + userId]);
	                location.href="index.html";
	            }).always(function(){
	             facebookLoggingIn = false;	            	  
	            });
            }
        }).fail(function(msg){
        	facebookLoggingIn = false;
        	alert('Unable to log the Facebook account into TinyNews due to an unknown error. Please try again later.');
        });
	}
	
	function registerOrLoginFBUser(facebook_auth_response){
		  $.ajax({
		    url: "/php/facebook_authenticate.php",
		    type: "POST",
		    data: {facebook_id : facebook_auth_response.id},
		    dataType: "html"
		  }).done(function(userId){
			  var userPicUrl = 'http://graph.facebook.com/'+facebook_auth_response.username+'/picture?type=large';
			  $('#userPhoto').attr('src', userPicUrl);
              TN.services.registerCustomer(userId, 'qwe', facebook_auth_response.name, "",userPicUrl).done(function(msg){
                  if ((msg == null) || (msg === null)) {
                	  //This Facebook account is already registered with Grab Your Towel.
                	  loginFBUser(userId, userPicUrl);
                  }
                  else if ((msg[0] == true) || (typeof msg[0] === "number")) {
                	  loginFBUser(userId, userPicUrl);
                  }
                  else {
                	  facebookLoggingIn = false;
                      alert ("Error occured during the registration process.");
                  }
              }).fail(function(msg){
                  if ((msg == null) || (msg === null)) {
                	  facebookLoggingIn = false;
                	  alert ("Error occured during the registration process. Please try again later.");
                  }
                  else {
                      jQuery.each( msg, function(key, value){ if (key == "status") regfail = value; });
                      if (regfail == 409) {
                      	loginFBUser(userId, userPicUrl);
                      }
                      else {
                    	  facebookLoggingIn = false;
                    	  alert ("Error occured during the registration process. Please try again later.");
                      }
                  };
              });
		  }).fail(function(userId){
			    alert('Error occured while authenticating your Facebook account.  Please try again later');
			    facebookLoggingIn = false;
		  });
		}
	
//	$fb.login = function(){
//		var buttonElem = $('#fbLogin');
//		if( facebookLoggingIn === true ){
//			return;
//		}
//		//buttonElem.text('Logging in...');
//		facebookLoggingIn = true;
//		FB.login(function(response) {
//			if (response.authResponse) {
//				FB.api('/me', function(response) {
////	        var facebook_id = response.id;
//	        getAuthenticate(response);
//				});
//			} else {
//				// $('#fbLogin').text('Login via Facebook');
//				facebookLoggingIn = false;
//				// cancelled
//			}
//		},{scope : 'email'});
//	};
	
	$fb.registerOrLogin = function(){
		var buttonElem = $('#fbRegister');
		if( facebookLoggingIn === true ){
			return;
		}
		
		facebookLoggingIn = true;
		FB.login(function(response) {
			if (response.authResponse) {
				FB.api('/me', function(response) {
					registerOrLoginFBUser(response);
				});
			} else {
				facebookLoggingIn = false;
				// cancelled
			}
		},{scope : 'email'});		
	};
	
	function postToFacebook(postInfo, fnCallback){
		var postObject;
		
		if( !!postInfo.messageId ){
			postObject = {
				method:'feed',
				link:'http://www.tinynews.me?lightboxId=' + postInfo.messageId,
				picture:postInfo.url,
				name:postInfo.headline
			};			
		} else {
			postObject = {
					method:'feed',
					link:'http://www.tinynews.me?npId=' + postInfo.npId,
					picture:postInfo.url,
					name:postInfo.headline
				};						
		}
		
		FB.ui(postObject, fnCallback);
//		FB.api('/me/og.posts',
//				'post',
//				{
//					object:url,
//					message:headline
//				}, fnCallback );		
	}
	
	$fb.post = function(postInfo, fnCallback){
		//Check if user already logged in facebook:
		FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {
			    // the user is logged in and has authenticated your
			    // app, and response.authResponse supplies
			    // the user's ID, a valid access token, a signed
			    // request, and the time the access token 
			    // and signed request each expire
			    var uid = response.authResponse.userID;
			    var accessToken = response.authResponse.accessToken;

			    postToFacebook(postInfo, fnCallback);
			  } else if (response.status === 'not_authorized') {
			    // the user is logged in to Facebook, 
			    // but has not authenticated your app
					FB.login(function(response) {
						if (response.authResponse) {
							postToFacebook(postInfo, function(fbResponse){
								FB.logout();
								fnCallback(fbResponse);
							});
						}
					},{scope : 'email'});		
			  } else {
			    // the user isn't logged in to Facebook.
					FB.login(function(response) {
						if (response.authResponse) {
							postToFacebook(postInfo, function(fbResponse){
								FB.logout();
								fnCallback(fbResponse);
							});
						}
					},{scope : 'email'});		
			  }
		});
	};
	
})(TN.FB);